<?php include 'header.php'; ?>
	<div class="login d-flex">
		<div class="login__image">
		</div>
		<div class="login__form">
			<div class="form__header">
				<img src="./images/logo-color.jpg" alt="">
				<p>Đăng nhập với tài khoản MBS</p>
			</div>
			<div class="form__body">
				<form>
					<div>
						<label for="username">Tên đăng nhập:</label>
						<input type="text" id="username" name="username" value="" placeholder="">
					</div>
					<div>
						<label for="password">Mật khẩu:</label>
						<input type="password" id="password" name="password" value="" placeholder="">
					</div>
					<div>
						<label><input class="custom-checkbox--1" type="checkbox" name="" value=""><span>Ghi nhớ trạng thái đăng nhập trên trình duyệt này</span></label>
					</div>
					<div>
						<button type="submit" class="btn btn--primary">Đăng nhập</button>
					</div>
				</form>
				<div class="txt-center">Hoặc</div>
				<div class="form__buttons">
					<a href="#" class="btn btn--blue-2">Đăng nhập qua facebook</a>
					<a href="#" class="btn btn--red">Đăng nhập qua google</a>
				</div>
				<div class="form__quen-mk txt-center">
					<a href="#">Mở tài khoản</a> |
					<a href="#">Quên mật khẩu</a>
				</div>
			</div>
			<div class="form__footer">
				<div class="footer__links">
					<a href="#">mbs.com.vn</a> |
					<a href="#">Trang chủ</a> |
					<a href="#">Dịch vụ chứng khoán</a>
				</div>
				<div class="footer__text">
					Hotline hỗ trợ: <strong>1900 9088</strong>
				</div>
			</div>
		</div>
	</div>
</body>
