<?php include 'header.php'; ?>
<div class="fixed-components">
	<?php include 'site-header.php'; ?>
	<?php include 'charts.php'; ?>
	<?php include 'stock-tables/header-market.php'; ?>
</div>

<div class="market stock-tables__table">

	<header class="market__sub-navigation">
		<ul class="sub-navigation__item is-active" data-tab="#chuyen-dong-thi-truong">
			<li><a class="market__sub__link active" href="#top-co-phieu">TOP cổ phiếu đóng góp vào INDEX</a></li>
			<li><a class="market__sub__link" href="#do-rong-thi-truong">Độ rộng thị trường</a></li>
			<li><a class="market__sub__link" href="#thanh-khoan">Thanh khoản</a></li>
			<li><a class="market__sub__link" href="#gt-kl">Giá trị và khối lượng giao dịch</a></li>
			<li><a class="market__sub__link" href="#von-hoa">Vốn hóa</a></li>
		</ul>
		<ul class="sub-navigation__item" data-tab="#chuyen-dong-to-chuc">
			<li><a class="market__sub__link" href="#nn">Nước ngoài mua bán ròng</a></li>
			<li><a class="market__sub__link" href="#tu-doanh">Tự doanh mua bán ròng</a></li>
		</ul>
		<ul class="sub-navigation__item" data-tab="#chuyen-dong-nganh">
			<li><a class="market__sub__link" href="#top-nganh">TOP ngành đóng góp vào INDEX</a></li>
			<li><a class="market__sub__link" href="#nn-nganh">Nước ngoài mua bán ròng theo ngành</a></li>
			<li><a class="market__sub__link" href="#tu-doanh-nganh">Tự doanh mua bán ròng theo ngành</a></li>
			<li><a class="market__sub__link" href="#gt-kl-nganh">Giá trị và khối lượng giao dịch theo ngành</a></li>
		</ul>
		<div class="sub-navigation__item" data-tab="#tin-hieu">
			<div class="backtest-options">
				<select class="js-my-select">
					<option value="bk-first-option">Ease of Movement (Backtest Performance)</option>
					<option value="bk-second-option">Model Reverse (Backtest Performance)</option>
				</select>
			</div>
			<div class="content-tin-hieu bk__option is-active" data-tab="bk-first-option">
				<h5><a class="init-backtest-modal" href=""><b>Ease of Movement (Backtest Performance)</b></a></h5>

				<table>
					<colgroup>
						<col></col>
						<col></col>
						<col></col>
						<col width="20%"></col>
						<col width="20%"></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
					</colgroup>
					<tr>
						<th>Ngày</th>
						<th>Sàn</th>
						<th>Mã CK</th>
						<th>Tên CK</th>
						<th>Phân loại CP</th>
						<th>Vốn hóa</th>
						<th>Giá hiện tại</th>
						<th>Beta30</th>
						<th>5% VAR</th>
						<th>Tín hiệu</th>
						<th>Backtest Performance</th>
					</tr>
					<?php for ( $rows = 1; $rows <= 10; $rows++ ) : ?>
					<tr>
						<td class="txt-center">08/07/2019</td>
						<td class="txt-center">HNX</td>
						<td class="txt-center txt-green">ACB</td>
						<td>Ngân hàng Á Châu</td>
						<td>Ít biến động</td>
						<td class="txt-right">36,18</td>
						<td class="txt-right">29,000</td>
						<td class="txt-right">0.88</td>
						<td class="txt-right txt-red">-2.44</td>
						<td class="txt-center">Bán</td>
						<td class="txt-center txt-green"><a class="init-chitiet-backtest-modal" href="" href="#">Xem</a></td>
					</tr>
					<?php endfor; ?>
				</table>
			</div>
			<div class="content-tin-hieu bk__option second-option" data-tab="bk-second-option">
				<h5><a class="init-backtest-modal" href="#"><b>Model Reverse (Backtest Performance)</b></a></h5>

				<table>
					<colgroup>
						<col></col>
						<col></col>
						<col></col>
						<col width="20%"></col>
						<col width="20%"></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
						<col></col>
					</colgroup>
					<tr>
						<th>Ngày</th>
						<th>Sàn</th>
						<th>Mã CK</th>
						<th>Tên CK</th>
						<th>Phân loại CP</th>
						<th>Vốn hóa</th>
						<th>Giá hiện tại</th>
						<th>Beta30</th>
						<th>5% VAR</th>
						<th>Tín hiệu</th>
						<th>Backtest Performance</th>
					</tr>
					<?php for ( $rows = 1; $rows <= 10; $rows++ ) : ?>
					<tr>
						<td class="txt-center">08/07/2019</td>
						<td class="txt-center">HNX</td>
						<td class="txt-center txt-green">ACB</td>
						<td>Ngân hàng Á Châu</td>
						<td>Ít biến động</td>
						<td class="txt-right">36,18</td>
						<td class="txt-right">29,000</td>
						<td class="txt-right">0.88</td>
						<td class="txt-right txt-red">-2.44</td>
						<td class="txt-center">Bán</td>
						<td class="txt-center txt-green"><a class="init-chitiet-backtest-modal" href="" href="#">Xem</a></td>
					</tr>
					<?php endfor; ?>
				</table>
			</div>
		</div>
	</header>

	

	<section class="market__description">
		<div class="market__description__item is-active" data-tab="#top-co-phieu">
			<h2>TOP cổ phiếu đóng góp vào INDEX</h2>
			<p>Biểu đồ thể hiện thông tin điểm ảnh hưởng và % ảnh hưởng của 30 cổ phiếu tác động nhất tới tăng giảm điểm của Bộ chỉ số, trong đó 15 mã ảnh hưởng chiều tăng và 15 mã ảnh hưởng chiều giảm.</p>
		</div>
		<div class="market__description__item" data-tab="#do-rong-thi-truong">
			<h2>Độ rộng thị trường</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
		</div>
		<div class="market__description__item" data-tab="#thanh-khoan">
			<h2>Thanh khoản</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
		</div>
		<div class="market__description__item" data-tab="#nn">
			<h2>Nước ngoài mua bán ròng</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
		</div>
		<div class="market__description__item" data-tab="#tu-doanh">
			<h2>Tự doanh mua bán ròng</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
		</div>
		<div class="market__description__item" data-tab="#gt-kl">
			<h2>Giá trị và khối lượng giao dịch</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
		</div>
		<div class="market__description__item" data-tab="#von-hoa">
			<h2>Vốn hóa</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
		</div>
		<div class="market__description__item" data-tab="#top-nganh">
			<h2>TOP ngành đóng góp vào INDEX</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
		</div>
		<div class="market__description__item" data-tab="#nn-nganh">
			<h2>Nước ngoài mua bán ròng theo ngành</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
		</div>
		<div class="market__description__item" data-tab="#tu-doanh-nganh">
			<h2>Tự doanh mua bán ròng theo ngành</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
		</div>
		<div class="market__description__item" data-tab="#gt-kl-nganh">
			<h2>Giá trị và khối lượng giao dịch theo ngành</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
		</div>
	</section>

	<section class="market__section is-active" data-tab="#top-co-phieu">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>

	</section>

	<section class="market__section" data-tab="#do-rong-thi-truong">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#thanh-khoan">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#nn">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng mua: <span class="txt-green">8888 tỷ</span>&emsp; Tổng bán:<span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#tu-doanh">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng mua: <span class="txt-green">8888 tỷ</span>&emsp; Tổng bán:<span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#gt-kl">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giá trị giao dịch</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#gia-tri">Giá trị giao dịch</a></li>
					<li><a href="#khoi-luong">Khối lượng giao dịch</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng: <span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#von-hoa">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#top-nganh">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#nn-nganh">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng mua: <span class="txt-green">8888 tỷ</span>&emsp; Tổng bán:<span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#tu-doanh-nganh">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng mua: <span class="txt-green">8888 tỷ</span>&emsp; Tổng bán:<span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#gt-kl-nganh">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giá trị giao dịch</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#gia-tri">Giá trị giao dịch</a></li>
					<li><a href="#khoi-luong">Khối lượng giao dịch</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
			<div class="total">
				Tổng: <span class="txt-green">8888 tỷ</span>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>

	<section class="market__section" data-tab="#tin-hieu">
		<div class="market__filter">
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">HOSE</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#hose">HOSE</a></li>
					<li><a href="#hnx">HNX</a></li>
					<li><a href="#upcom">UPCOM</a></li>
					<li><a href="#vn30">VN30</a></li>
					<li><a href="#hnx30">HNX30</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giá trị giao dịch</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#gia-tri">Giá trị giao dịch</a></li>
					<li><a href="#khoi-luong">Khối lượng giao dịch</a></li>
				</ul>
			</div>
			<div class="market__select dropdown">
				<span class="dropdown-toggle"><span class="market__select__text">Giao dịch khớp lệnh</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu dropdown-menu--lg">
					<li><a href="#khop-lenh">Giao dịch khớp lệnh</a></li>
					<li><a href="#khop-lenh-thoa-thuan">Giao dịch khớp lệnh và thỏa thuận</a></li>
				</ul>
			</div>
		</div>
		<div class="market__chart">
			<img src="./images/market/1.svg">
		</div>
	</section>
</div>


<?php 
include './modals/popup-backtest.php';
include './modals/chitiet-backtest.php';
include 'footer.php';
?>
