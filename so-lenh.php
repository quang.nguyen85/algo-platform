<div id="so-lenh-popup" class="footer-nav-popup so-lenh-popup is-hidden lenh-trong-ngay">
	<div class="footer-nav-popup__bar d-flex space-between align-center">
		<span class="footer-nav-popup__title">
			<i class="fas fa-book"></i>Sổ lệnh
		</span>
		<span class="footer-nav-popup__close">
			<i class="fas fa-angle-down"></i>
		</span>
	</div>
	<div class="footer-nav-popup__header d-flex space-between">
		<div>
			<button class="btn btn--red so-lenh__huy" type="button">Hủy</button>
			<button class="btn btn--primary so-lenh__kich-hoat" type="button">Kích hoạt</button>
		</div>

		<div class="so-lenh__tk so-lenh__select has-submenu dropdown">
			<a class="dropdown-toggle" title="Danh mục"><span>Tài khoản</span><i class="fas fa-angle-down"></i></a>
			<ul class="dropdown-menu--mua dropdown-menu">
				<li><a href="#">4608121</a></li>
				<li><a href="#">4608128</a></li>
			</ul>
		</div>
	</div>
	<div class="footer-nav-popup__body">
		<div class="so-lenh__khop so-lenh__select has-submenu dropdown">
			<a class="dropdown-toggle" title="Danh mục"><span>Khớp một phần</span><i class="fas fa-angle-down"></i></a>
			<ul class="dropdown-menu--mua dropdown-menu">
				<li><a href="#">Khớp một phần</a></li>
				<li><a href="#">Khớp hai phần</a></li>
			</ul>
		</div>
		<div class="so-lenh__lenh so-lenh__select has-submenu dropdown">
			<a class="dropdown-toggle" title="Danh mục"><span>Lệnh trong ngày</span><i class="fas fa-angle-down"></i></a>
			<ul class="dropdown-menu--mua dropdown-menu">
				<li><a href="#" data-value="lenh-trong-ngay">Lệnh trong ngày</a></li>
				<li><a href="#" data-value="lenh-phien-ke-tiep">Lệnh phiên kế tiếp</a></li>
				<li><a href="#" data-value="gio-lenh">Giỏ lệnh</a></li>
			</ul>
		</div>

		<table class="so-lenh__table">
			<colgroup>
				<col style="width: 23px">
				<col style="width: 29px">
				<col style="width: 40px">
				<col style="width: 52px">
				<col style="width: 57px">
				<col style="width: 43px">
				<col style="width: 33px">
				<col class="so-lenh__table__edit">
				<col class="so-lenh__table__copy">
			<thead>
				<th><input class="dropdown__checkall" type="checkbox" name="" value=""></th>
				<th>Lệnh</th>
				<th>Mã CK</th>
				<th>KL</th>
				<th>KL Khớp</th>
				<th>Giá</th>
				<th>TT</th>
				<th class="so-lenh__table__edit"></th>
				<th class="so-lenh__table__copy"></th>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>

	<div class="footer-nav-popup__footer">

	</div>
</div>