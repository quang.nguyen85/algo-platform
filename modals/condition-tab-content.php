<div id="tab-content-1" class="condition__tab-content custom-scrollbar active">
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>ROE 4 quý gần nhất (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>ROE 4 quý so với TB ngành (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>ROA 4 quý gần nhất (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>ROA 4 quý so với TB ngành (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>PE 4 quý gần nhất (lần)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>PE 4 quý so với TB ngành (lần)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>EPS quý gần nhất (VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>EPS 4 quý gần nhất (VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>EPS 4 quý gần nhất (VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>EPS 4 quý gần nhất (VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>EPS 4 quý gần nhất (VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>EPS 4 quý gần nhất (VND)</span></label>
</div>

<div id="tab-content-2" class="condition__tab-content custom-scrollbar">
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Thay đổi Giá so với Phiên gần nhất (1000 VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Thay đổi Giá so với Phiên gần nhất (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Tổng Khối lượng giao dịch (10 CP)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Tổng Giá trị giao dịch (Triệu VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Thay đổi KLGD so với KLTB 10 phiên (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Thay đổi KLGD so với KLTB 20 phiên (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>GTGD TB 20 phiên gần nhất (Tỷ VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Thay đổi Giá so với Đỉnh 52 tuần (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Thay đổi Giá so với Đáy 52 tuần (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Tỷ suất Sinh lời 5 năm hoặc Từ ngày đầu tiên niêm yết (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Thay đổi Giá so với X phiên (%)</span></label>
</div>

<div id="tab-content-3" class="condition__tab-content custom-scrollbar">
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Giá trị mua khối ngoại (Triệu VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Giá trị bán khối ngoại (Triệu VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Giá trị mua bán ròng của Khối ngoại</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Giá trị mua khối ngoại so với TB 5 phiên trước (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Giá trị mua khối ngoại so với TB 10 phiên trước (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Giá trị mua khối ngoại so với TB 20 phiên trước (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Giá trị bán khối ngoại so với TB 5 phiên trước (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Giá trị bán khối ngoại so với TB 20 phiên trước (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Giá trị mua bán ròng khối ngoại so với TB x phiên (%)</span></label>
</div>

<div id="tab-content-4" class="condition__tab-content custom-scrollbar">
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>BETA Năm gần nhất (lần)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Tỷ lệ nợ xấu (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Tỷ lệ Nợ xấu bao gồm VAMC (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Tỷ lệ Dự phòng nợ xấu/ Tổng nợ xấu (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Tỷ lệ Dự phòng nợ xấu/ Tổng nợ xấu bao gồm VAMC (%)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>NIM (%)</span></label>
</div>


<div id="tab-content-5" class="condition__tab-content custom-scrollbar">
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Đường SMA (10)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Đường SMA (20)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Đường SMA (50)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Đường SMA (200)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Mô hình Hộp Darvas</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Đường MACD</span></label>
</div>

<div id="tab-content-6" class="condition__tab-content custom-scrollbar">
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Vốn hóa (Tỷ VND)</span></label>
	<label class="tab-content__item custom-checkbox custom-checkbox--2"><input id="<?php echo uniqid(); ?>" type="checkbox" name="" value=""><span>Thời gian niêm yết (Năm)</span></label>
</div>