<div class="modal modal--chitiet-backtest" id="chitiet-backtest">
	<div class="modal__background"></div>
	<div class="modal__body">
		
		<h3>Ease of Movement</h3>
		
		<p>Kết quả backtest cho max ABC</p>
		<table>
			<tr>
				<th>Năm</th>
				<th>Tín hiệu mua</th>
				<th>Tín hiệu bán</th>
				<th>Lệnh mua</th>
				<th>Lệnh bán </th>
				<th>Lệnh lãi</th>
				<th>Lệnh lỗ</th>
				<th>Cắt lỗ</th>
				<th>Chốt lãi</th>
				<th>Return (%)</th>
				<th>VNI Return (%)</th>
			</tr>
			<?php for ( $rows = 1; $rows <= 10; $rows++ ) : ?>
			<tr>
				<td class="txt-center">2014</td>
				<td class="txt-center txt-green">3368</td>
				<td class="txt-center txt-green">8888</td>
				<td class="txt-center txt-green">4355</td>
				<td class="txt-center txt-green">578</td>
				<td class="txt-center txt-green">356.5</td>
				<td class="txt-center txt-green">4365</td>
				<td class="txt-center txt-green">346</td>
				<td class="txt-center txt-green">2.44</td>
				<td class="txt-center txt-green">42</td>
				<td class="txt-center txt-green"><a href="#">64</a></td>
			</tr>
			<?php endfor; ?>
		</table>
		<button type="button" class="btn btn--red modal__close--btn">Đóng</button>
	</div>
</div>


