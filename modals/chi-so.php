<div class="modal modal--chi-so" id="chi-so-modal">
	<div class="modal__background"></div>
	<div class="modal__body">
		<h3 class="txt-center">Lựa chọn chỉ số</h3>
		<div class="chi-so-items">
			<div class="chi-so-item">
				<div class="ten-san">HOSE</div>
				<div class="ten-chi-so">
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="HOSE" value="VNXALL"><span>VNXALL</span></label>
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="HOSE" value="VNI"><span>VNI</span></label>
					<label class="custom-checkbox custom-checkbox--3"><input data-default="true" checked type="checkbox" name="HOSE" value="VN30"><span>VN30</span></label>
					<label class="custom-checkbox custom-checkbox--3"><input data-default="true" checked type="checkbox" name="HOSE" value="VNI50"><span>VNI50</span></label>
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="HOSE" value="VNI100"><span>VNI100</span></label>
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="HOSE" value="VNMID"><span>VNMID</span></label>
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="HOSE" value="VNSML"><span>VNSML</span></label>
				</div>
			</div>
			<div class="chi-so-item">
				<div class="ten-san">Phái sinh</div>
				<div class="ten-chi-so">
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="phai-sinh" value="VN30F1M"><span>VN30F1M</span></label>
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="phai-sinh" value="VN30F2M"><span>VN30F2M</span></label>
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="phai-sinh" value="VN30F1Q"><span>VN30F1Q</span></label>
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="phai-sinh" value="VN30F2Q"><span>VN30F2Q</span></label>
				</div>
			</div>
			<div class="chi-so-item">
				<div class="ten-san">HNX</div>
				<div class="ten-chi-so">
					<label class="custom-checkbox custom-checkbox--3"><input data-default="true" checked type="checkbox" name="HNX" value="HNX"><span>HNX</span></label>
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="HNX" value="HNX30"><span>HNX30</span></label>
				</div>
			</div>
			<div class="chi-so-item">
				<div class="ten-san">UPCOM</div>
				<div class="ten-chi-so">
					<label class="custom-checkbox custom-checkbox--3"><input data-default="true" checked type="checkbox" name="UPCOM" value="UPCOM"><span>UPCOM</span></label>
				</div>
			</div>
			<div class="chi-so-item">
				<div class="ten-san">GDTT</div>
				<div class="ten-chi-so">
					<label class="custom-checkbox custom-checkbox--3"><input type="checkbox" name="GDTT" value="GDTT"><span></span></label>
				</div>
			</div>
		</div>
		<button type="button" id="set-chi-so-default" class="btn btn--primary">Thiết lập mặc định</button>
		<button type="button" class="btn btn--no-bg modal__close--btn">Lưu</button>
		<button type="button" class="btn btn--red modal__close--btn">Đóng</button>
		<button type="button" class="modal__close"><i class="fas fa-times"></i></button>
	</div>
</div>
