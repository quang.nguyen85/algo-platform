<div class="modal modal--filter" id="bo-loc-modal">
	<div class="modal__background"></div>
	<div class="modal__body">
		<div class="filter__header d-flex flex-wrap">
			<div class="filter__name filter--left">
				<input type="text" name="" value="" placeholder="Nhập tên bộ lọc">
			</div>
			<div class="filter__save filter--right">
				<div class="filter__total-stock"><span class="number">181</span> mã thỏa mãn</div>
				<div class="filter__buttons">
					<button class="btn btn--primary buttons__save" type="button">Lưu và xem kết quả</button>
					<button class="btn btn--no-bg buttons__reset" type="button">Làm lại</button>
				</div>
			</div>
		</div>
		<div class="filter__body">
			<div class="filter__inputs d-flex flex-wrap">
				<div class="input__search filter--left">
					<input type="text" placeholder="Tìm kiếm điều kiện lọc">
					<button type="submit"><i class="fas fa-search"></i></button>
				</div>
				<div class="input__selects filter--right d-flex flex-wrap">
					<select name="filter_nganh">
						<option value="">Lọc theo sàn</option>
						<option value="">HOSE</option>
						<option value="">HNX</option>
					</select>
					<select name="filter_san">
						<option value="">Lọc theo ngành</option>
						<option value="">Ngành kiến trúc xây dựng</option>
						<option value="">Ngành sản xuất chế biến</option>
					</select>
				</div>
			</div>
			<div class="filter__conditions d-flex flex-wrap">
				<div class="filter--left d-flex flex-wrap">
					<div class="condition__tab-items">
						<a href="#tab-content-1" class="tab__item active">Phân đoạn thị trường <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-2" class="tab__item">Giá và khối lượng <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-3" class="tab__item">Khối ngoại <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-4" class="tab__item">Chỉ số cơ bản <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-5" class="tab__item">Nhóm tiêu chí đặc thù <i class="fas fa-angle-right"></i></a>
						<a href="#tab-content-6" class="tab__item">Tín hiệu kỹ thuật <i class="fas fa-angle-right"></i></a>
					</div>
					<?php include 'condition-tab-content.php' ?>
				</div>
				<div class="filter__edit-condition custom-scrollbar filter--right">

				</div>
			</div>
		</div>
		<button type="button" class="modal__close"><i class="fas fa-times"></i></button>
	</div>
</div>
