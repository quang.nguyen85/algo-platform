<?php include 'header.php'; ?>
<div class="fixed-components">
	<?php include 'site-header.php'; ?>
	<?php include 'charts.php'; ?>
	<?php include 'stock-tables/header-no-tabs.php'; ?>
</div>

<div class="code-details stock-tables__table">
	<nav class="code-details__menu">
		<ul>
			<li><a title="Tổng quan" href="#thong-tin-chung"><i class="fas fa-home"></i> <span class="code-details__menu-item">Tổng quan</span></a></li>
			<li><a title="Giao dịch" href="#giao-dich" class="is-active"><i class="fas fa-chart-bar"></i> <span class="code-details__menu-item">Giao dịch</span></a></li>
			<li><a title="Tài chính" href="#tai-chinh"><i class="fas fa-search-dollar"></i> <span class="code-details__menu-item">Tài chính</span></a></li>
			<li><a title="Sự kiện" href="#su-kien"><i class="far fa-calendar-alt"></i> <span class="code-details__menu-item">Sự kiện</span></a></li>
			<li><a title="Định giá" href="#dinh-gia"><i class="fas fa-calculator"></i> <span class="code-details__menu-item">Định giá</span></a></li>
			<li><a title="So sánh" href="#so-sanh"><i class="fab fa-stack-exchange"></i> <span class="code-details__menu-item">So sánh</span></a></li>
			<li><a title="Kỹ thuật" href="#ky-thuat"><i class="fas fa-chart-line"></i> <span class="code-details__menu-item">Kỹ thuật</span></a></li>
			<li><a title="Tín hiệu" href="#tin-hieu"><i class="far fa-bell"></i> <span class="code-details__menu-item">Tín hiệu</span></a></li>
			<li><a title="Khuyến nghị" style="height: 40px;" href="#bao-cao" class="code-details__menu__large"><i class="far fa-file-alt"></i> <span class="code-details__menu-item">Khuyến<br>nghị</span></a></li>
			<li><a title="Báo cáo xếp hạng" href="bao-cao-xep-hang.php" target="_blank" class="code-details__menu__large"><i class="fas fa-list-ol"></i> <span class="code-details__menu-item">Báo cáo<br>xếp hạng</span></a></li>
		</ul>
		<a href="#" class="code-details__close"><i class="fas fa-angle-left"></i></a>
	</nav>
	<div class="code-details__content">
		<div class="code-details__section" data-tab="#thong-tin-chung">
			<div class="ttc__code">KDC</div>
			<div class="ttc__company">Công ty Cổ phần thực phẩm đông lạnh Kido - UPCOM</div>
			<div class="ttc__brief code-details__row">
				<div class="code-details__half">
					<table>
						<tr>
							<td>Ngành: Thực phẩm và đồ uống</td>
						</tr>
						<tr>
							<td>Vốn hóa: 1.268 tỷ VNĐ</td>
						</tr>
						<tr>
							<td>Ngày niêm yết: 06/04/2009</td>

						</tr>
					</table>
				</div>
				<div class="code-details__half">
					
				</div>
			</div>
			<div class="code-details__row">
				<div class="code-details__half">
					<div class="code-details__chart"><img src="./images/chi-tiet-ma/1.svg"></div>
				</div>
				<div class="code-details__half">
					<div class="code-details__chart"><img src="./images/chi-tiet-ma/2.svg"></div>
				</div>
			</div>
			<div class="code-details__row">
				<div class="code-details__chart"><img src="./images/chi-tiet-ma/3.svg"></div>
			</div>
		</div>
		<div class="code-details__section is-active" data-tab="#giao-dich">
			<div class="gd__tabs">
				<a href="#giao-dich-trong-ngay" class="is-active">Giao dịch trong ngày</a>
				<a href="#lich-su-giao-dich">Lịch sử giao dịch</a>
			</div>
			<div class="gd__tab is-active" data-tab="#giao-dich-trong-ngay">
				<div class="code-details__row">
					<div class="code-details__chart"><img src="./images/chi-tiet-ma/4.svg"></div>
				</div>
				<div class="code-details__row">
					<div class="code-details__chart"><img src="./images/chi-tiet-ma/5.svg"></div>
				</div>
				<div class="code-details__row">
					<div class="code-details__chart"><img src="./images/chi-tiet-ma/6.svg"></div>
				</div>
				<div class="code-details__row">
					<div class="code-details__half">
						<div class="code-details__chart"><img src="./images/chi-tiet-ma/7.svg"></div>
					</div>
					<div class="code-details__half">
						<div class="code-details__chart"><img src="./images/chi-tiet-ma/8.svg"></div>
					</div>
				</div>
				<div class="code-details__row">
					<div class="code-details__half">
						<div class="code-details__chart"><img src="./images/chi-tiet-ma/9.svg"></div>
					</div>
					<div class="code-details__half">
						<div class="code-details__chart"><img src="./images/chi-tiet-ma/10.svg"></div>
					</div>
				</div>
			</div>
			<div class="gd__tab" data-tab="#lich-su-giao-dich">
				<div class="code-details__row">
					<div class="code-details__chart"><img src="./images/chi-tiet-ma/11.svg"></div>
				</div>
				<div class="code-details__row gd__history">
					<div class="gd__history__filter">
						Từ ngày
						<input type="text" class="date-picker">
						Đến ngày
						<input type="text" class="date-picker">
						<button class="btn btn--primary">Tìm kiếm</button>
					</div>
					<table>
						<tr>
							<th rowspan="2">Ngày</th>
							<th rowspan="2">Giá đóng cửa</th>
							<th rowspan="2">Thay đổi</th>
							<th rowspan="2">Giá tham chiếu</th>
							<th rowspan="2">NN mua bán ròng</th>
							<th rowspan="2">Tự doanh<br>mua bán ròng</th>
							<th colspan="2">Khớp lệnh</th>
							<th colspan="2">Thỏa thuận</th>
						</tr>
						<tr>
							<th>KLGD</th>
							<th>GTGD</th>
							<th>KLGD</th>
							<th>GTGD</th>
						</tr>
						<?php for ( $i = 1; $i <= 4; $i++ ) : ?>
							<tr>
								<td class="txt-green">26/04/2019</td>
								<td class="txt-green txt-right">877</td>
								<td class="txt-green txt-right">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
								<td class="txt-green txt-right">868.6</td>
								<td class="txt-green txt-right">152</td>
								<td class="txt-green txt-right">152</td>
								<td class="txt-green txt-right">10,989</td>
								<td class="txt-green txt-right">8,900,958</td>
								<td class="txt-green txt-right">11,989</td>
								<td class="txt-green txt-right">8,900,958</td>
							</tr>
							<tr>
								<td class="txt-red">26/04/2019</td>
								<td class="txt-red txt-right">877</td>
								<td class="txt-red txt-right">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
								<td class="txt-red txt-right">868.6</td>
								<td class="txt-red txt-right">152</td>
								<td class="txt-red txt-right">152</td>
								<td class="txt-red txt-right">10,989</td>
								<td class="txt-red txt-right">8,900,958</td>
								<td class="txt-red txt-right">11,989</td>
								<td class="txt-red txt-right">8,900,958</td>
							</tr>
						<?php endfor; ?>
					</table>
				</div>
			</div>
		</div>
		<div class="code-details__section" data-tab="#tin-hieu">Tín hiệu</div>
		<div class="code-details__section code-details__section--no-padding-top" data-tab="#tai-chinh">
			<div class="tc__header">
				<div class="tc__dropdown dropdown">
					<span class="dropdown-toggle"><span class="tc__dropdown__text">Mô hình dự báo</span> <span class="tc__dropdown__icon"><i class="fas fa-angle-down"></i></span></span>
					<ul class="dropdown-menu">
						<li><a href="#mo-hinh-du-bao">Mô hình dự báo</a></li>
						<li><a href="#phan-phoi-loi-suat">Phân phối lợi suất - Chỉ số quán tính</a></li>
						<li><a href="#eps">EPS và cổ tức</a></li>
						<li><a href="#chi-so-tang-truong">Chỉ số tăng trưởng - Chỉ số giá trị - Chỉ số chất lượng</a></li>
						<hr>
						<li><a href="#phan-tich-dupont">Phân tích Dupont</a></li>
						<li><a href="#kha-nang-sinh-loi">Khả năng sinh lời</a></li>
						<li><a href="#chi-so-hieu-qua">Chỉ số hiệu quả hoạt động</a></li>
						<li><a href="#chi-so-thanh-khoan">Chỉ số thanh khoản</a></li>
						<li><a href="#chi-so-thanh-toan">Chỉ số thanh toán dài hạn</a></li>
						<hr>
						<li><a href="#ket-qua-kinh-doanh">Kết quả kinh doanh</a></li>
						<li><a href="#can-doi-ke-toan">Cân đối kế toán</a></li>
						<li><a href="#dong-tien">Dòng tiền</a></li>
					</ul>
				</div>
				<div class="tc__period">
					<a href="#quarter" class="is-active"><i class="fas fa-chart-bar"></i> Quý</a>
					<a href="#year"><i class="fas fa-chart-line"></i> Năm</a>
				</div>
			</div>
			<div class="tc__section tc__section--first is-active" data-tab="#mo-hinh-du-bao">
				<h3 class="tc__heading">Mô hình dự báo</h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/12.svg"></div>
					</div>
					<div class="tc__right">
						<table class="tc__period-data is-active" data-tab="#quarter">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">Quý<br>4/2018</th>
								<th class="cell-highlight">Quý<br>3/2018</th>
								<th class="cell-highlight">Quý<br>2/2018</th>
								<th class="cell-highlight">Quý<br>1/2018</th>
							</tr>
							<tr>
								<td>ALGO Ranking <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
							<tr>
								<td>Magic Formula Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
							<tr>
								<td>F-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
							<tr>
								<td>Z-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
							<tr>
								<td>M-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
							<tr>
								<td>C-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
						</table>
						<table class="tc__period-data" data-tab="#year">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">2018</th>
								<th class="cell-highlight">2017</th>
								<th class="cell-highlight">2016</th>
								<th class="cell-highlight">2015</th>
							</tr>
							<tr>
								<td>ALGO Ranking <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
							<tr>
								<td>Magic Formula Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
							<tr>
								<td>F-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
							<tr>
								<td>Z-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
							<tr>
								<td>M-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
							<tr>
								<td>C-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
								<td class="txt-right">56.95</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#phan-phoi-loi-suat">
				<h3 class="tc__heading">Phân phối lợi suất - Chỉ số quán tính</h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/13.svg"></div>
					</div>
					<div class="tc__right">
						<table>
							<tr>
								<th class="cell-highlight txt-left">Sức mạnh tương đương <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></th>
								<th class="cell-highlight"></th>
							</tr>
							<tr>
								<td>1 tháng</td>
								<td class="txt-right">17.4%</td>
							</tr>
							<tr>
								<td>3 tháng</td>
								<td class="txt-right">17.4%</td>
							</tr>
							<tr>
								<td>1 năm</td>
								<td class="txt-right txt-red">-10.4%</td>
							</tr>
							<tr>
								<th class="cell-highlight txt-left">Thay đổi khối lượng</th>
								<th class="cell-highlight"></th>
							</tr>
							<tr>
								<td>TB 10 ngày / TB 3 tháng</td>
								<td class="txt-right">17.4%</td>
							</tr>
							<tr>
								<td>TB 10 ngày / TB 6 tháng</td>
								<td class="txt-right">17.4%</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#eps">
				<h3 class="tc__heading">EPS và cổ tức <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/12.svg"></div>
					</div>
					<div class="tc__right">
						<table class="tc__period-data is-active" data-tab="#quarter">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">Quý<br>4/2018</th>
								<th class="cell-highlight">Quý<br>3/2018</th>
								<th class="cell-highlight">Quý<br>2/2018</th>
								<th class="cell-highlight">Quý<br>1/2018</th>
							</tr>
							<tr>
								<td>EPS</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>DPS</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>TL đảm bảo cổ tức</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
						<table class="tc__period-data" data-tab="#year">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">2018</th>
								<th class="cell-highlight">2017</th>
								<th class="cell-highlight">2016</th>
								<th class="cell-highlight">2015</th>
							</tr>
							<tr>
								<td>EPS</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>DPS</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>TL đảm bảo cổ tức</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#chi-so-tang-truong">
				<h3 class="tc__heading">Chỉ số Tăng trưởng - Chỉ số Giá trị - Chỉ số chất lượng</h3>
				<div class="tc__row">
					<div class="tc__half">
						<table>
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th colspan="2" class="cell-highlight txt-left">Tăng trưởng <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></th>
								<th class="cell-highlight">Ngành</th>
								<th class="cell-highlight">Thị trường</th>
							</tr>
							<tr>
								<td>PE</td>
								<td class="txt-right">17.4%</td>
								<td class="txt-right">17.4%</td>
								<td class="txt-right">17.4%</td>
							</tr>
							<tr>
								<td>Tăng trưởng EPS</td>
								<td class="txt-right">17.4%</td>
								<td class="txt-right">17.4%</td>
								<td class="txt-right">17.4%</td>
							</tr>
							<tr>
								<td>Lợi tức cổ tức</td>
								<td class="txt-right txt-red">17.4%</td>
								<td class="txt-right txt-red">17.4%</td>
								<td class="txt-right txt-red">17.4%</td>
							</tr>
							<tr>
								<th colspan="4" class="cell-highlight txt-left">Niên độ gần nhất vs. năm trước</th>
							</tr>
							<tr>
								<td>Tăng trưởng doanh thu</td>
								<td class="txt-right">17.4%</td>
								<td class="txt-right">17.4%</td>
								<td class="txt-right">17.4%</td>
							</tr>
							<tr>
								<td>Tăng trưởng EPS</td>
								<td class="txt-right txt-red">17.4%</td>
								<td class="txt-right txt-red">17.4%</td>
								<td class="txt-right txt-red">17.4%</td>
							</tr>
							<tr>
								<th colspan="4" class="cell-highlight txt-left">Tăng trưởng kép hàng năm <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></th>
							</tr>
							<tr>
								<td>Tăng trưởng doanh thu</td>
								<td class="txt-right">17.4%</td>
								<td class="txt-right">17.4%</td>
								<td class="txt-right">17.4%</td>
							</tr>
							<tr>
								<td>Tăng trưởng EPS</td>
								<td class="txt-right txt-red">17.4%</td>
								<td class="txt-right txt-red">17.4%</td>
								<td class="txt-right txt-red">17.4%</td>
							</tr>
							<tr>
								<td>Tăng trưởng DPS <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right txt-red">17.4%</td>
								<td class="txt-right txt-red">17.4%</td>
								<td class="txt-right txt-red">17.4%</td>
							</tr>
						</table>
					</div>
					<div class="tc__half">
						<table>
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th colspan="2" class="cell-highlight txt-left">Giá trị <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></th>
								<th class="cell-highlight">Ngành</th>
								<th class="cell-highlight">Thị trường</th>
							</tr>
							<tr>
								<td>PB</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
							</tr>
							<tr>
								<td>Giá / giá trị sổ sách hữu hình</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
							</tr>
							<tr>
								<td>P/FCF <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right txt-red">17.4</td>
								<td class="txt-right txt-red">17.4</td>
								<td class="txt-right txt-red">17.4</td>
							</tr>
							<tr>
								<td>PS</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
							</tr>
							<tr>
								<td>EV/EBITDA <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
							</tr>
							<tr>
								<td>Tăng trưởng EPS</td>
								<td class="txt-right txt-red">17.4</td>
								<td class="txt-right txt-red">17.4</td>
								<td class="txt-right txt-red">17.4</td>
							</tr>
							<tr>
								<th colspan="4" class="cell-highlight txt-left">Chất lượng</th>
							</tr>
							<tr>
								<td>ROCE</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
							</tr>
							<tr>
								<td>ROE</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
							</tr>
							<tr>
								<td>Biên LN hoạt động</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
								<td class="txt-right">17.4</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#phan-tich-dupont">
				<h3 class="tc__heading">Phân tích Dupont <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/12.svg"></div>
					</div>
					<div class="tc__right">
						<table class="tc__period-data is-active" data-tab="#quarter">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">Quý<br>4/2018</th>
								<th class="cell-highlight">Quý<br>3/2018</th>
								<th class="cell-highlight">Quý<br>2/2018</th>
								<th class="cell-highlight">Quý<br>1/2018</th>
							</tr>
							<tr>
								<td>ROE</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Biên LN ròng</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Vòng quay tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Đòn bẩy tài chính</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
						<table class="tc__period-data" data-tab="#year">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">2018</th>
								<th class="cell-highlight">2017</th>
								<th class="cell-highlight">2016</th>
								<th class="cell-highlight">2015</th>
							</tr>
							<tr>
								<td>ROE</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Biên LN ròng</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Vòng quay tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Đòn bẩy tài chính</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#kha-nang-sinh-loi">
				<h3 class="tc__heading">Khả năng sinh lời <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/12.svg"></div>
					</div>
					<div class="tc__right">
						<table class="tc__period-data is-active" data-tab="#quarter">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">Quý<br>4/2018</th>
								<th class="cell-highlight">Quý<br>3/2018</th>
								<th class="cell-highlight">Quý<br>2/2018</th>
								<th class="cell-highlight">Quý<br>1/2018</th>
							</tr>
							<tr>
								<td>Biên LN gộp</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Biên LN hoạt động</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>ROA</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>ROE</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>ROCE</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
						<table class="tc__period-data" data-tab="#year">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">2018</th>
								<th class="cell-highlight">2017</th>
								<th class="cell-highlight">2016</th>
								<th class="cell-highlight">2015</th>
							</tr>
							<tr>
								<td>Biên LN gộp</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Biên LN hoạt động</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>ROA</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>ROE</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>ROCE</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#chi-so-hieu-qua">
				<h3 class="tc__heading">Chỉ số hiệu quả hoạt động</h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/12.svg"></div>
					</div>
					<div class="tc__right">
						<table class="tc__period-data is-active" data-tab="#quarter">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">Quý<br>4/2018</th>
								<th class="cell-highlight">Quý<br>3/2018</th>
								<th class="cell-highlight">Quý<br>2/2018</th>
								<th class="cell-highlight">Quý<br>1/2018</th>
							</tr>
							<tr>
								<td>Vòng quay tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Vòng quay khoản phải thu</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Vòng quay hàng tồn kho</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Vòng quay khoản phải trả</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
						<table class="tc__period-data" data-tab="#year">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">2018</th>
								<th class="cell-highlight">2017</th>
								<th class="cell-highlight">2016</th>
								<th class="cell-highlight">2015</th>
							</tr>
							<tr>
								<td>Vòng quay tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Vòng quay khoản phải thu</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Vòng quay hàng tồn kho</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Vòng quay khoản phải trả</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#chi-so-thanh-khoan">
				<h3 class="tc__heading">Chỉ số thanh khoản</h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/12.svg"></div>
					</div>
					<div class="tc__right">
						<table class="tc__period-data is-active" data-tab="#quarter">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">Quý<br>4/2018</th>
								<th class="cell-highlight">Quý<br>3/2018</th>
								<th class="cell-highlight">Quý<br>2/2018</th>
								<th class="cell-highlight">Quý<br>1/2018</th>
							</tr>
							<tr>
								<td>Chỉ số TT ngắn hạn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Chỉ số TT nhanh</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Chỉ số TT tiền mặt</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
						<table class="tc__period-data" data-tab="#year">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">2018</th>
								<th class="cell-highlight">2017</th>
								<th class="cell-highlight">2016</th>
								<th class="cell-highlight">2015</th>
							</tr>
							<tr>
								<td>Chỉ số TT ngắn hạn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Chỉ số TT nhanh</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Chỉ số TT tiền mặt</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#chi-so-thanh-toan">
				<h3 class="tc__heading">Chỉ số thanh toán dài hạn</h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/12.svg"></div>
					</div>
					<div class="tc__right">
						<table class="tc__period-data is-active" data-tab="#quarter">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">Quý<br>4/2018</th>
								<th class="cell-highlight">Quý<br>3/2018</th>
								<th class="cell-highlight">Quý<br>2/2018</th>
								<th class="cell-highlight">Quý<br>1/2018</th>
							</tr>
							<tr>
								<td>Nợ / Tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Tiền mặt / Tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Nợ ròng / Tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
						<table class="tc__period-data" data-tab="#year">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">2018</th>
								<th class="cell-highlight">2017</th>
								<th class="cell-highlight">2016</th>
								<th class="cell-highlight">2015</th>
							</tr>
							<tr>
								<td>Nợ / Tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Tiền mặt / Tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Nợ ròng / Tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#ket-qua-kinh-doanh">
				<h3 class="tc__heading">Kết quả kinh doanh <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/12.svg"></div>
					</div>
					<div class="tc__right">
						<table class="tc__period-data is-active" data-tab="#quarter">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">Quý<br>4/2018</th>
								<th class="cell-highlight">Quý<br>3/2018</th>
								<th class="cell-highlight">Quý<br>2/2018</th>
								<th class="cell-highlight">Quý<br>1/2018</th>
							</tr>
							<tr>
								<td>Doanh thu thuần</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>LN gộp</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Lãi / lỗ từ HĐKD</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>LN ròng</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
						<table class="tc__period-data" data-tab="#year">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">2018</th>
								<th class="cell-highlight">2017</th>
								<th class="cell-highlight">2016</th>
								<th class="cell-highlight">2015</th>
							</tr>
							<tr>
								<td>Doanh thu thuần</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>LN gộp</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Lãi / lỗ từ HĐKD</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>LN ròng</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#can-doi-ke-toan">
				<h3 class="tc__heading">Cân đối kế toán <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/12.svg"></div>
					</div>
					<div class="tc__right">
						<table class="tc__period-data is-active" data-tab="#quarter">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">Quý<br>4/2018</th>
								<th class="cell-highlight">Quý<br>3/2018</th>
								<th class="cell-highlight">Quý<br>2/2018</th>
								<th class="cell-highlight">Quý<br>1/2018</th>
							</tr>
							<tr>
								<td>Tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Tài sản ngắn hạn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Tài sản dài hạn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Tổng nợ phải trả</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Nợ phải trả ngắn hạn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Nợ phải trả dài hạn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Tổng vốn chủ sở hữu</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
						<table class="tc__period-data" data-tab="#year">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">2018</th>
								<th class="cell-highlight">2017</th>
								<th class="cell-highlight">2016</th>
								<th class="cell-highlight">2015</th>
							</tr>
							<tr>
								<td>Tổng tài sản</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Tài sản ngắn hạn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Tài sản dài hạn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Tổng nợ phải trả</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Nợ phải trả ngắn hạn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Nợ phải trả dài hạn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Tổng vốn chủ sở hữu</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="tc__section" data-tab="#dong-tien">
				<h3 class="tc__heading">Dòng tiền <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></h3>
				<div class="tc__row">
					<div class="tc__left">
						<div class="tc__chart"><img src="./images/chi-tiet-ma/12.svg"></div>
					</div>
					<div class="tc__right">
						<table class="tc__period-data is-active" data-tab="#quarter">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">Quý<br>4/2018</th>
								<th class="cell-highlight">Quý<br>3/2018</th>
								<th class="cell-highlight">Quý<br>2/2018</th>
								<th class="cell-highlight">Quý<br>1/2018</th>
							</tr>
							<tr>
								<td>CFO</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>CFI</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>CFF</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Chi tiêu vốn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>FCF</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
						<table class="tc__period-data" data-tab="#year">
							<colgroup>
								<col class="tc__col-noi-dung">
								<col>
								<col>
								<col>
								<col>
							</colgroup>
							<tr>
								<th class="cell-highlight">Nội dung</th>
								<th class="cell-highlight">2018</th>
								<th class="cell-highlight">2017</th>
								<th class="cell-highlight">2016</th>
								<th class="cell-highlight">2015</th>
							</tr>
							<tr>
								<td>CFO</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>CFI</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>CFF</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>Chi tiêu vốn</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
							<tr>
								<td>FCF</td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-down"></i><span class="tc__small">10.50%</span></td>
								<td class="txt-right">56.95<br><i class="fas fa-caret-up"></i><span class="tc__small">10.50%</span></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="code-details__section" data-tab="#su-kien">
			<div class="sukien__tabs sukien__header">
				<a href="#sukien-first-tab" class="is-active">Sự kiện</a>
				<a href="#sukien-second-tab" class="">GD CĐ Lớn và nội bộ</a>
			</div>
			<div class="sukien__tab is-active" data-tab="#sukien-first-tab">
				<table >
					<colgroup>
						<col>
						<col style="width: 50%">
						<col>
						<col>
						<col>
					</colgroup>	
					<tbody>
						<tr>
							<th >Ngày thông báo</th>
							<th >Mô tả sự kiện</th>
							<th >Ngày GDKDQ</th>
							<th >Ngày đăng ký</br> cuối cùng</th>
							<th >thực hiện</th>
						</tr>
						<tr>
							<td class="txt-center">29/05/2019</td>
							<td class="">Phát hành cổ phiếu Phát hành cổ phiếu Phát hành cổ phiếu Phát hành cổ phiếu Phát hành cổ phiếu Phát hành cổ phiếu</td>
							<td class="txt-center">04/06/2019</td>
							<td class="txt-center">05/06/2019</td>
							<td class="txt-center">20/06/2019</td>
						</tr>
						<tr>
							<td class="txt-center">29/05/2019</td>
							<td class="">Phát hành cổ phiếu</td>
							<td class="txt-center">04/06/2019</td>
							<td class="txt-center">05/06/2019</td>
							<td class="txt-center">20/06/2019</td>
						</tr>
						<tr>
							<td class="txt-center">29/05/2019</td>
							<td class="">Phát hành cổ phiếu</td>
							<td class="txt-center">04/06/2019</td>
							<td class="txt-center">05/06/2019</td>
							<td class="txt-center">20/06/2019</td>
						</tr>
						<tr>
							<td class="txt-center">29/05/2019</td>
							<td class="">Phát hành cổ phiếu</td>
							<td class="txt-center">04/06/2019</td>
							<td class="txt-center">05/06/2019</td>
							<td class="txt-center">20/06/2019</td>
						</tr>
						<tr>
							<td class="txt-center">29/05/2019</td>
							<td class="">Phát hành cổ phiếu</td>
							<td class="txt-center">04/06/2019</td>
							<td class="txt-center">05/06/2019</td>
							<td class="txt-center">20/06/2019</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="sukien__tab sk-second-tab" data-tab="#sukien-second-tab">
				<table>
					<tbody>
						<tr>
							<th>Ngày thông báo</th>
							<th>Giao dịch</th>
							<th>Người</br>giao dịch</th>
							<th>Chức vụ</th>
							<th>Người</br>liên quan</th>
							<th>Mối</br>quan hệ</th>
							<th>KL</br>đăng ký</th>
							<th>KL đã</br>thực hiện</th>
							<th>KL</br>năm giữ</th>
							<th>Ngày</br>bắt đầu</th>
							<th>Ngày</br>kết thúc</th>
							<th>Trạng thái</th>
						</tr>
						<tr>
							<td class="txt-center">25/09/2019</td>
							<td class="txt-center">Mua</td>
							<td class="txt-center">Trần Đình Long</td>
							<td class="txt-center">Chủ tịch HĐQT</td>
							<td class="txt-center">Vũ THị Hiền</td>
							<td class="txt-center">Vợ</td>
							<td class="txt-center">1000000</td>
							<td class="txt-center"></td>
							<td class="txt-center"></td>
							<td class="txt-center">21/06/2019</td>
							<td class="txt-center">19/07/2019</td>
							<td class="txt-center">Đăng ký</td>
						</tr>
						<tr>
							<td class="txt-center">25/09/2019</td>
							<td class="txt-center">Mua</td>
							<td class="txt-center">Trần Đình Long</td>
							<td class="txt-center">Chủ tịch HĐQT</td>
							<td class="txt-center">Vũ THị Hiền</td>
							<td class="txt-center">Vợ</td>
							<td class="txt-center">1000000</td>
							<td class="txt-center"></td>
							<td class="txt-center"></td>
							<td class="txt-center">21/06/2019</td>
							<td class="txt-center">19/07/2019</td>
							<td class="txt-center">Đăng ký</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="code-details__section" data-tab="#bao-cao">Báo cáo</div>
		<div class="code-details__section" data-tab="#dinh-gia">
			<div class="dg__options">
				<select class="js-my-select">
					<option value="dg-first-option">Phương pháp dự phóng lợi nhuận ròng</option>
					<option value="dg-second-option">Dự phóng báo cáo kết quả kinh doanh</option>
				</select>
			</div>
			<div class="dg__option is-active" data-tab="dg-first-option">
				<p>Đơn vị: Tỷ VNĐ</p>
				<table>
					<tbody>
						<tr>
							<th>Năm</th>
							<th>2014</th>
							<th>2015</th>
							<th>2016</th>
							<th>2017</th>
							<th>2018</th>
							<th>4 quý gần nhất</th>
							<th>Q1 2019</th>
						</tr>
						<tr>
							<td class="td-title">Lợi nhuận ròng</td>
							<td class="txt-green txt-center">4,585</td>
							<td class="txt-green txt-center">5,322</td>
							<td class="txt-green txt-center">6,851</td>
							<td class="txt-green txt-center">9,111</td>
							<td class="txt-green txt-center">14,622</td>
							<td class="txt-green txt-center">15,861</td>
							<td class="txt-green txt-center">4,711</td>
						</tr>
					</tbody>
				</table>
				<div class="du-phong-loi-nhuan">
					<table>
						<tbody>
							<tr>
								<td class="txt-right">Dự phóng lợi nhận dòng cho</td>
								<td>
									<select class="option-table">
										<option>Quý tiếp theo</option>
										<option>4 quý tiếp theo</option>
										<option>Năm 2019</option>
									</select>
								</td>
								<td></td>
							</tr>
							<tr>
								<td class="txt-right">Kỳ vọng lợi nhuận ròng tăng/giảm (%)</td>
								<td class=""><input type="" name="" placeholder="Nhập kỳ vọng lợi nhuận tại đây" /></td>
								<td class="btn-PE txt-left"><button class="">Tính PE Forward</button></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="code-details__row">
					<div class="code-details__chart"><img src="./images/chi-tiet-ma/4.svg"></div>
				</div>
			</div>
			<div class="dg__option second-option" data-tab="dg-second-option">
				<p>Đơn vị: Tỷ VNĐ</p>
				<table>
					<colgroup>
						<col style="width: 17%">
						<col>
						<col>
						<col>
						<col>
						<col>
						<col>
						<col style="width: 17%">
						<col>
						<col>
						<col>
					</colgroup>	
					<tbody>
						<tr>
							<th rowspan="2">Báo cáo kết quả kinh doanh</th>
							<th colspan="2">Năm 2019</th>
							<th colspan="2">4 quý gần nhất</th>
							<th colspan="2">Q1 2019</th>
							<th colspan="4">Dự phóng</th>
						</tr>
						<tr>
							<th>Giá trị</th>
							<th>% Doanh thu</th>
							<th>Giá trị</th>
							<th>% Doanh thu</th>
							<th>Giá trị</th>
							<th>% Doanh thu</th>
							<th>Thay đổi về</th>
							<th>% Thay đổi</th>
							<th>Số dự phóng</th>
							<th>% Doanh thu</th>
						</tr>
						<tr>
							<td class="">Dự phóng cho</td>
							<td colspan="6"></td>
							<td colspan="2">
								<select class="option-table">
									<option>Quý tiếp theo</option>
									<option>4 quý tiếp theo</option>
									<option>Năm 2019</option>
								</select>
							</td>
							<td class="btn-PE" colspan="2"><button>Tính PE Forward</button></td>
						</tr>
						<tr>
							<td>Doanh thu thuần</td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td>
								<select class="option-table">
									<option>Số tuyệt đối</option>
								</select>
							</td>
							<td>
								<input type="" name="" />
							</td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
						</tr>
						<tr>
							<td>Giá vốn hàng bán </td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td>
								<select class="option-table">
									<option>Tỷ trọng doanh thu</option>
								</select>
							</td>
							<td>
								<input type="" name="" />
							</td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
						</tr>
						<tr>
							<td>Lợi nhuận gộp</td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
						</tr>
						<tr>
							<td>Lãi/(lỗ) từ hoạt động tài chính </td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td>
								<select class="option-table">
									<option>Tỷ trọng doanh thu</option>
								</select>
							</td>
							<td>
								<input type="" name="" />
							</td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
						</tr>
						<tr>
							<td>Lãi/(lỗ) từ công ty liên doanh</td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td>
								<select class="option-table">
									<option>Tỷ trọng doanh thu</option>
								</select>
							</td>
							<td>
								<input type="" name="" />
							</td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
						</tr>
						<tr>
							<td>Chi phí bán hàng và quản lý doanh nghiệp</td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td>
								<select class="option-table">
									<option>Tỷ trọng doanh thu</option>
								</select>
							</td>
							<td>
								<input type="" name=""  />
							</td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
						</tr>
						<tr>
							<td>Lãi/(lỗ) từ Hoạt động kinh doanh</td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
						</tr>
						<tr>
							<td>Lãi/(lỗ) khác</td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="">
								<select class="option-table">
									<option>Tỷ trọng doanh thu</option>
								</select>
							</td>
							<td>
								<input type="" name="" />
							</td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
						</tr>
						<tr>
							<td>Lợi nhuận trước thuế</td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
						</tr>
						<tr>
							<td>Chi phí thuế TNDN</td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td>
								<select class="option-table">
									<option>Tỷ trọng doanh thu</option>
								</select>
							</td>
							<td>
								<input type="" name=""/>
							</td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
						</tr>
						<tr>
							<td>Lợi nhuận sau thuế</td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
						</tr>
						<tr>
							<td>Số CPLHPQ (Triệu CP)</td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
							<td>
								<select class="option-table">
									<option>Số tuyệt đối</option>
								</select>
							</td>
							<td>
								<input type="" name=""/>
							</td>
							<td class="txt-green txt-right"></td>
							<td class="txt-green txt-right"></td>
						</tr>
						<tr>
							<td>EPS 4 quý gần nhất</td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
							<td class="txt-red txt-right"></td>
						</tr>
					</tbody>
				</table>
				<div class="code-details__row">
					<div class="code-details__chart"><img src="./images/chi-tiet-ma/4.svg"></div>
				</div>
			</div>
		</div>
		<div class="code-details__section" data-tab="#so-sanh">
			<div class="so-sanh">
				<div class="head-so-sanh">
					<form>
						<input id="so-sanh-view-code" type="" name="" placeholder="Chọn công ty so sánh" />
						<button id="submit-view-code">Chọn</button>
					</form>
				</div>
				<table id="bang-so-sanh">
					<thead class="h-table">
						<tr class="th">
							<th colspan="2">Danh sách gợi ý</th>
						</tr>
					</thead>
					<tbody>
						<tr class="h-table-so-sanh">
							<td colspan="2">Chỉ tiêu nhanh</td>
						</tr>
						<tr>
							<td data-key="von_hoa" class="col-27">Vốn hóa</td>
							<td class="txt-center col-10">Tỷ VNĐ</td>
						</tr>
						<tr>
							<td data-key="doanh_thu" class="col-27">Doanh thu</td>
							<td class="txt-center col-10">Tỷ VNĐ</td>
						</tr>
						<tr>
							<td data-key="loi_nhuan_rong" class="col-27">Lợi nhuận ròng</td>
							<td class="txt-center col-10">Tỷ VNĐ</td>
						</tr>
						<tr>
							<td data-key="cfo" class="col-27">CFO <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">Tỷ VNĐ</td>
						</tr>
						<tr>
							<td data-key="fcf" class="col-27">FCF <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">Tỷ VNĐ</td>
						</tr>
						<tr>
							<td data-key="tong_tai_san" class="col-27">Tổng tài sản</td>
							<td class="txt-center col-10">Tỷ VNĐ</td>
						</tr>
						<tr>
							<td data-key="tong_no_phai_tra" class="col-27">Tổng nợ phải trả</td>
							<td class="txt-center col-10">Tỷ VNĐ</td>
						</tr>
						<tr>
							<td data-key="von_chu_so_huu" class="col-27">Vốn chủ sở hữu</td>
							<td class="txt-center col-10">Tỷ VNĐ</td>
						</tr>
						<tr class="h-table-so-sanh">
							<td colspan="2">Mô hình dự báo</td>
						</tr>
						<tr>
							<td data-key="mbs_ranking" class="col-27">ALGO Ranking <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-red txt-right col-10"></td>
						</tr>
						<tr>
							<td data-key="magic_formula_score" class="col-27">Magic Formula Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-green txt-right col-10"></td>
						</tr>
						<tr>
							<td data-key="f_score" class="col-27">F-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-red txt-right col-10"></td>
						</tr>
						<tr>
							<td data-key="z_score" class="col-27">Z-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-green txt-right col-10"></td>
						</tr>
						<tr>
							<td data-key="m_score" class="col-27">M-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-red txt-right col-10"></td>
						</tr>
						<tr>
							<td data-key="m_score" class="col-27">C-Score <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-red txt-right col-10"></td>
						</tr>
						<tr class="h-table-so-sanh">
							<td colspan="2">Giao dịch và cung cầu</td>
						</tr>
						<tr>
							<td data-key="so_huu_nuoc_ngoai" class="col-27">Sở hữu nước ngoài</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="room_con_lai" class="col-27">Room còn lại</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="khoi_luong_giao_dich">Khối lượng giao dịch</td>
							<td class="txt-center col-10">Cổ phiếu</td>
						</tr>
						<tr>
							<td data-key="gia_tri_giao_dich" class="col-27">Giá trị giao dịch</td>
							<td class="txt-center col-10">Triệu VNĐ</td>
						</tr>
						<tr>
							<td data-key="khoi_ngoai_mua_dong" class="col-27">GTGD Khối ngoại mua ròng <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">Triệu VNĐ</td>
						</tr>
						<tr>
							<td data-key="tu_doanh_mua_dong" class="col-27">GTGD Tự doanh mua ròng <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">Triệu VNĐ</td>
						</tr>
						<tr>
							<td data-key="gtgd_5phien" class="col-27">Bình quân GTGD 5 phiên</td>
							<td class="txt-center col-10">Triệu VNĐ</td>
						</tr>
						<tr>
							<td data-key="gtgd_10phien" class="col-27">Bình quân GTGD 10 phiên</td>
							<td class="txt-center col-10">Triệu VNĐ</td>
						</tr>
						<tr>
							<td data-key="gtgd_20phien" class="col-27">Bình quân GTGD 20 phiên</td>
							<td class="txt-center col-10">Triệu VNĐ</td>
						</tr>
						<tr>
							<td data-key="bien_dong_gia_5phien" class="col-27">Mức biến động giá 5 phiên</td>
							<td class="txt-center col-10">Triệu VNĐ</td>
						</tr>
						<tr>
							<td data-key="bien_dong_gia_10phien" class="col-27">Mức biến động giá 10 phiên</td>
							<td class="txt-center col-10">Triệu VNĐ</td>
						</tr>
						<tr>
							<td data-key="bien_dong_gia_20phien" class="col-27">Mức biến động giá 20 phiên</td>
							<td class="txt-center col-10">Triệu VNĐ</td>
						</tr>
						<tr class="h-table-so-sanh">
							<td colspan="2">Chỉ số giá trị (trailing 4Q)</td>
						</tr>
						<tr>
							<td data-key="eps" class="col-27">EPS <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">VNĐ</td>
						</tr>
						<tr>
							<td data-key="pe" class="col-27">PE <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr>
							<td data-key="pb" class="col-27">PB <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr>
							<td data-key="p_tangible_bv" class="col-27">P/Tangible BV <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">lần</td>
						</tr>
						<tr>
							<td data-key="ps" class="col-27">PS <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr>
							<td data-key="p_fcf" class="col-27">P/FCF <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr>
							<td data-key="tev_ebitda" class="col-27">EV/EBITDA <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr class="h-table-so-sanh">
							<td colspan="2">Chỉ số tăng trưởng (trailing 4Q)</td>
						</tr>
						<tr>
							<td data-key="tang_truong_eps" class="col-27">Tăng trưởng EPS</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="loi_tuc_co_tuc" class="col-27">Lợi tức cổ tức <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="" class="txt-bold" class="col-27" style="border-right: none">Trailing 4Q so với năm trước: <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10" style="border-left: none"></td>
						</tr>
						<tr>
							<td data-key="tang_truong_doanh_thu_svnt" class="col-27">- Tăng trưởng doanh thu</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="tang_truong_eps_svnt" class="col-27">- Tăng trưởng EPS</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td class="col-27 txt-bold" style="border-right: none">Tăng trưởng kép hàng năm (3 năm): <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td style="border-left: none"  class="col-10"></td>
						</tr>
						<tr>
							<td data-key="tang_truong_doanh_thu_kep" class="col-27">- Tăng trưởng doanh thu</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="tang_truong_eps_kep" class="col-27">- Tăng trưởng EPS</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="tang_truong_dps_kep" class="col-27">- Tăng trưởng DPS</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr class="h-table-so-sanh">
							<td colspan="2">Chỉ số chất lượng</td>
						</tr>
						<tr>
							<td data-key="roa" class="col-27">ROA <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="roe" class="col-27">ROE <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="roce" class="col-27">ROCE <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="bien_loi_nhuan_hd" class="col-27">Biên lợi nhuận hoạt động <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr class="h-table-so-sanh">
							<td colspan="2">Chỉ số quán tính và chỉ báo kĩ thuật</td>
						</tr>
						<tr>
							<td data-key="" class="txt-bold col-27" style="border-right: none">Sức mạnh tương đương (%): <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="col-10" style="border-left:none"></td>
						</tr>
						<tr>
							<td data-key="suc_manh_tuong_duong_1thang" class="col-27">- 1 tháng</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="suc_manh_tuong_duong_3thang" class="col-27">- 3 tháng</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="suc_manh_tuong_duong_1nam" class="col-27">- 1 năm</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="" class="txt-bold col-27" style="border-right: none">Thay đổi khối lượng (%): <i class="tooltip fas fa-info-circle" data-tippy-content="Giải thích" tabindex="0"></i></td>
							<td class="txt-center col-10" style="border-left: none"></td>
						</tr>
						<tr>
							<td data-key="thay_doi_kl_1_5ngay" class="col-27">- 1 ngày vs 5 ngày</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="thay_doi_kl_10_3thang" class="col-27">- 10 ngày vs 3 tháng</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="thay_doi_kl_10_6thang" class="col-27">- 10 ngày vs 6 tháng</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="gia_cao52" class="col-27">% Giá cao 52 tuần</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="ma50" class="col-27">% MA 50</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr>
							<td data-key="ma200" class="col-27">% MA 200</td>
							<td class="txt-center col-10">%</td>
						</tr>
						<tr class="h-table-so-sanh">
							<td colspan="2">Chỉ số hiệu quả hoạt động</td>
						</tr>
						<tr>
							<td data-key="vq_tong_tai_san" class="col-27">Vòng quay tổng tài sản</td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr>
							<td data-key="vq_khoan_phai_thu" class="col-27">Vòng quay khoản phải thu</td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr>
							<td data-key="vq_khoan_phai_tra" class="col-27">Vòng quay khoản phải trả</td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr>
							<td data-key="vq_hang_ton_kho" class="col-27">Vòng quay hàng tồn kho</td>
							<td class="txt-center col-10">lần</td>
						</tr>
						<tr class="h-table-so-sanh">
							<td colspan="2">Chỉ số thanh khoản</td>
						</tr>
						<tr>
							<td data-key="cstt_ngan_han" class="col-27">Chỉ số thanh toán ngắn hạn </td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr>
							<td data-key="cstt_nhanh" class="col-27">Chỉ số thanh toán nhanh</td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr>
							<td data-key="cstt_tien_mat" class="col-27">Chỉ số thanh toán tiền mặt</td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr class="h-table-so-sanh">
							<td colspan="2">Chỉ số thanh toán dài hạn</td>
						</tr>
						<tr>
							<td data-key="don_bay_tai_chinh" class="col-27">Đòn bẩy tài chính</td>
							<td class="txt-center col-10">Lần</td>
						</tr>
						<tr>
							<td data-key="tldbcp_vay_lai" class="col-27">Tỉ lệ đảm bảo chi phí lãi vay</td>
							<td class="txt-center col-10">Lần</td>
						</tr>
					</tbody>
				</table>
			</div>	
		</div>
		<div class="code-details__section" data-tab="#ky-thuat">Khuyến nghị</div>
	</div>
	<aside class="code-details__side">
		<div class="code-details__info">
			<div class="code-details__title">
				<span class="code-details__code">KDC</span>
				<span class="txt-green"><i class="fas fa-caret-up"></i></span>
				<span class="code-details__price">29</span>
				<span class="code-details__diff txt-green">+0.5%<br>+0.50</span>
			</div>
			<div class="code-details__company">Công ty Cổ phần thực phẩm đông lạnh Kido | UPCOM</div>
			<table class="code-details__prices">
				<tr>
					<th>Giá tham chiếu</th>
					<td><span class="txt-yellow">28.9</span></td>
				</tr>
				<tr>
					<th>Giá mở cửa - Giá BQ</th>
					<td><span class="txt-red">0</span> - <span class="txt-green">28.9</span></td>
				</tr>
				<tr>
					<th>Cao - Thấp 52 tuần</th>
					<td><span class="txt-red">16.5</span> - <span class="txt-green">28.9</span></td>
				</tr>
				<tr>
					<th>MA 50 - MA 200</th>
					<td><span class="txt-red">21.5</span> - <span class="txt-green">28.9</span></td>
				</tr>
			</table>
		</div>

		<div class="code-details__tables">
			<div class="code-details__tables__nav">
				<a href="#khop-lenh" class="is-active">Khớp lệnh</a>
				<a href="#buoc-gia">Bước giá</a>
			</div>
			<div class="code-details__tables__tab is-active" data-tab="#khop-lenh">
				<table>
					<tr>
						<th>Thời gian</th>
						<th>Giá</th>
						<th>KL</th>
						<th>Tổng KL</th>
					</tr>
					<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
						<tr>
							<td class="txt-green txt-right">15:05:20</td>
							<td class="txt-green txt-right">95.00</td>
							<td class="txt-green txt-right">39</td>
							<td class="txt-green txt-right">153</td>
						</tr>
					<?php endfor; ?>
				</table>
			</div>
			<div class="code-details__tables__tab" data-tab="#buoc-gia">
				<table>
					<tr>
						<th colspan="2">Dư mua</th>
						<th colspan="2">Dư bán</th>
					</tr>
					<tr>
						<th>KL</th>
						<th>Giá</th>
						<th>Giá</th>
						<th>KL</th>
					</tr>
					<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
						<tr>
							<td class="txt-green txt-right">35</td>
							<td class="txt-green txt-right">85.75</td>
							<td class="txt-green txt-right">87.50</td>
							<td class="txt-green txt-right">36</td>
						</tr>
					<?php endfor; ?>
					<tr>
						<td class="txt-center code-details__tables__total" colspan="2">350</td>
						<td class="txt-center code-details__tables__total" colspan="2">360</td>
					</tr>
				</table>
				<div class="code-details__tables__stats">
					<div class="bg-green-2" style="width: 23%">23%</div>
					<div class="bg-red-2" style="width: 77%">77%</div>
				</div>
			</div>
		</div>
	</aside>
</div>

<?php include 'footer.php'; ?>


