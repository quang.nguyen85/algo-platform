<?php include 'header.php'; ?>

<div class="fixed-components">
	<?php include 'site-header.php'; ?>
	<?php include 'charts.php'; ?>
	<?php include 'stock-tables/header-no-tabs.php'; ?>
</div>

<div class="qltk stock-tables__table">
	<header class="qltk__filter">
		<div class="qltk__filter__left">
			Tài khoản
			<div class="qltk__select dropdown">
				<span class="dropdown-toggle"><span class="qltk__select__text">123456</span> <i class="fas fa-angle-down"></i></span>
				<ul class="dropdown-menu">
					<li><a href="#123456">123456</a></li>
					<li><a href="#345678">345678</a></li>
					<li><a href="#567890">567890</a></li>
				</ul>
			</div>
		</div>
		<div class="qltk__filter__right">
			<div class="qltk__types">
				<a href="#bao-cao-tai-san" class="is-active">Báo cáo tài sản</a>
				<a href="#danh-muc-tai-san">Danh mục tài sản</a>
			</div>
		</div>
	</header>
	<section class="qltk__section qltk__bc is-active" data-tab="#bao-cao-tai-san">
		<div class="qltk__row">
			<div class="qltk__left">
				<table>
					<colgroup>
						<col>
						<col class="qltk-col-number">
					</colgroup>
					<tr>
						<th>I. TỔNG GIÁ TRỊ CHỨNG KHOÁN</th>
						<th class="txt-right">999,999,999,999</th>
					</tr>
					<tr>
						<th>II. TIỀN (1-2-3+4+5)</th>
						<th class="txt-right">88,413</th>
					</tr>
					<tr>
						<td>1. Số dư</td>
						<td class="txt-right">88,413</td>
					</tr>
					<tr>
						<td>2. Mua chờ khớp</td>
						<td class="txt-right">0</td>
					</tr>
					<tr>
						<td>3. Mua đã khớp chờ thanh toán</td>
						<td class="txt-right">0</td>
					</tr>
					<tr>
						<td>4. Tiền bán chờ về (T0, T1, T2)</td>
						<td class="txt-right">0</td>
					</tr>
					<tr>
						<td>5. Giá trị quyền chờ về (tạm tính)(*)</td>
						<td class="txt-right">0</td>
					</tr>
					<tr>
						<th>III. TỔNG NỢ DVTC (6+7+8)</th>
						<th class="txt-right">0</th>
					</tr>
					<tr>
						<td>6. Dư nợ gốc</td>
						<td class="txt-right">0</td>
					</tr>
					<tr>
						<td>7. Lãi/Phí dịch vụ tài chính</td>
						<td class="txt-right">0</td>
					</tr>
					<tr>
						<td>8. Tiền đã ứng</td>
						<td class="txt-right">0</td>
					</tr>
					<tr>
						<th>IV. TÀI SẢN RÒNG THỰC</th>
						<th class="txt-right">88,413</th>
					</tr>
				</table>
			</div>
			<div class="qltk__right">
				<table>
					<colgroup>
						<col>
						<col class="qltk-col-number">
						<col>
						<col class="qltk-col-number">
					</colgroup>
					<tr>
						<th>THÔNG TIN TÀI KHOẢN DỊCH VỤ TÀI CHÍNH</th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
					<tr>
						<td>Tỷ lệ call của MBS</td>
						<td class="txt-right">-</td>
						<td>Sức mua</td>
						<td class="txt-right">84,123</td>
					</tr>
					<tr>
						<td>Tỷ lệ Force Sell của MBS</td>
						<td class="txt-right">-</td>
						<td>GTDM tính QTRR</td>
						<td class="txt-right">-</td>
					</tr>
					<tr>
						<td>Tỷ lệ sau mua</td>
						<td class="txt-right">-</td>
						<td>TSR tính QTRR</td>
						<td class="txt-right">-</td>
					</tr>
					<tr>
						<td>Tỷ lệ lãi vay dịch vụ</td>
						<td class="txt-right">-</td>
						<td>Tỷ lệ ký quỹ hiện tại (TSR/GTDM)</td>
						<td class="txt-right">-</td>
					</tr>
					<tr>
						<td>Hạn mức DVTC</td>
						<td class="txt-right">-</td>
						<td>Hạn mức FAL còn lại</td>
						<td class="txt-right">-</td>
					</tr>
					<tr>
						<td>Hạn mức FAL</td>
						<td class="txt-right">-</td>
						<td>Nợ FAL T0</td>
						<td class="txt-right">-</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>Nợ FAL T1</td>
						<td class="txt-right">-</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>Nợ FAL T2</td>
						<td class="txt-right">-</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>Nợ FAL quá T2</td>
						<td class="txt-right">-</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="qltk__notes">
			<em>Chú thích:</em><br>
			- Đơn vị giá: 1000 đồng<br>
			- Đơn vị khối lượng: 1 cổ phiếu<br>
			- Đơn vị tiền: đồng<br>
			- (*)Giá trị quyền chờ về: Là giá trị tạm tính theo nguyên tắc của MBS <br> <span style="padding-left: 10px;">MBS không chịu trách nhiệm về tính chính xác và hiệu lực của thông tin quyền</span>
		</div>
	</section>
	<section class="qltk__section qltk__dm" data-tab="#danh-muc-tai-san">
		<h3>Số dư</h3>
		<table>
			<tr>
				<th rowspan="2">Số dư</th>
				<th rowspan="2">Sức mua</th>
				<th rowspan="2">Số tiền<br>có thể rút</th>
				<th rowspan="2">Mua chờ khớp</th>
				<th colspan="2">T0</th>
				<th colspan="2">T1</th>
				<th colspan="2">T2</th>
				<th rowspan="2">Thao tác</th>
			</tr>
			<tr>
				<th>Mua</th>
				<th>Bán</th>
				<th>Mua</th>
				<th>Bán</th>
				<th>Mua</th>
				<th>Bán</th>
			</tr>
			<tr>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-center font-bold txt-white-1">Mua</td>
			</tr>
			<tr>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-right">2309921</td>
				<td class="txt-center font-bold txt-white-1">Mua</td>
			</tr>
		</table>
		<h3>Số dư chứng khoán</h3>
		<table class="dsl__dropdown__table dsl__dropdown__table--dmts">
			<colgroup>
				<col class="qltk__col-stt">
				<col>
				<col>
				<col class="qltk__col-kq">
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
			</colgroup>
			<thead>
				<tr>
					<th rowspan="2">STT</th>
					<th rowspan="2">Mã CK</th>
					<th rowspan="2">Trạng thái</th>
					<th rowspan="2">Tỷ lệ<br>ký quỹ</th>
					<th rowspan="2">Tổng KL</th>
					<th rowspan="2">Quyền chờ về</th>
					<th rowspan="2">KL có thể<br>giao dịch</th>
					<th colspan="2">T0</th>
					<th colspan="2">T1</th>
					<th colspan="2">T2</th>
					<th rowspan="2">Giá BQ</th>
					<th rowspan="2">Giá TT</th>
					<th rowspan="2">Giá trị</th>
					<th rowspan="2">Giá trị<br>TT</th>
					<th rowspan="2">Giá trị<br>lãi lỗ</th>
					<th rowspan="2">% Lãi/Lỗ</th>
					<th rowspan="2">Thao tác</th>
				</tr>
				<tr>
					<th>KL.Mua</th>
					<th>KL.Bán</th>
					<th>KL.Mua</th>
					<th>KL.Bán</th>
					<th>KL.Mua</th>
					<th>KL.Bán</th>
				</tr>
			</thead>
			<tbody>
				<?php for ( $i = 1; $i <= 20; $i++ ) : ?>
					<tr>
						<td class="txt-center"><?= $i ?></td>
						<td class="txt-center txt-ma txt-green">NVT</td>
						<td class="txt-right">Kích hoạt</td>
						<td class="txt-right">20%</td>
						<td class="txt-right">45,312</td>
						<td class="txt-right">52.326</td>
						<td class="txt-right">12,654</td>
						<td class="txt-right">261</td>
						<td class="txt-right">125</td>
						<td class="txt-right">261</td>
						<td class="txt-right">125</td>
						<td class="txt-right">261</td>
						<td class="txt-right">125</td>
						<td class="txt-right">21.5</td>
						<td class="txt-right">20.5</td>
						<td class="txt-right">25.6</td>
						<td class="txt-right">26.1</td>
						<td class="txt-right">15.60</td>
						<td class="txt-right">12%</td>
						<td class="txt-center font-bold txt-white-1">Mua</td>
					</tr>
				<?php endfor; ?>
			</tbody>
		</table>
		<div class="dsl__dropdown__footer d-flex flex-wrap">
			<div class="dsl__dropdown__pagination">
				<span class="first"><i class="fas fa-step-backward"></i></span>
				<span class="prev"><i class="fas fa-chevron-left"></i></span>
				<input class="dropdown-pagination__page-number" type="number" min="1" value="1">
				<span class="next"><i class="fas fa-chevron-right"></i></span>
				<span class="last"><i class="fas fa-step-forward"></i></span>
				<input class="dropdown-pagination__row-number" type="number" min="1" value="5">
			</div>
		</div>

		<div class="qltk__notes">
			<em>Chú thích:</em><br>
			- Đơn vị giá: 1000 đồng<br>
			- Đơn vị khối lượng: 1 cổ phiếu<br>
			- Đơn vị tiền: đồng
		</div>
	</section>
</div>

<?php include 'footer.php'; ?>
