<?php include 'header.php'; ?>

<div class="fixed-components">
	<?php $menu_title = 'Phái sinh'; ?>
	<?php include 'site-header.php'; ?>
	<?php include 'charts.php'; ?>
	<?php include 'stock-tables/header-no-tabs.php'; ?>
</div>

<div class="stock-tables__table">
	<div class="stock-table table--active phai-sinh">
		<?php
		include( 'stock-tables/phai-sinh/header.php' );
		include( 'stock-tables/phai-sinh/body.php' );
		?>
	</div>
</div>

<?php include 'footer-phai-sinh.php'; ?>