	<footer class="site-footer d-flex space-between flex-wrap">
		<div class="copyright">Copyright 2019 MBS. All rights reserved.</div>
		<div class="site-footer__right d-flex">
			<nav class="footer-navigation d-flex">
				<a class="footer-navigation__link" href="#" title=""><i class="fas fa-chart-line"></i>Bảng giá</a>
				<a class="footer-navigation__link" href="#so-lenh-popup" title=""><i class="fas fa-book"></i>Sổ lệnh</a>
				<a class="footer-navigation__link" href="#danh-muc-popup" title=""><i class="far fa-list-alt"></i>Danh mục</a>
				<a class="footer-navigation__link" href="#tai-san-popup" title=""><i class="far fa-money-bill-alt"></i>Tài sản</a>
			</nav>
			<a class="chat-icon tooltip" data-tippy-content="Chat với chúng tôi" href="javascript:void(Tawk_API.toggle())"><i class="fas fa-comments"></i></a>
		</div>
	</footer>

	<?php
	include './modals/filter.php';
	include './modals/chi-so.php';
	include './danh-muc-moi.php';
	include './dat-lenh-popup.php';
	include './so-lenh.php';
	?>
	<script src="https://cdn.jsdelivr.net/npm/css-vars-ponyfill@2"></script>
	<script src="//code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="./js/slick.min.js"></script>
	<script src="./js/nouislider.min.js"></script>
	<script src="./js/popper.min.js"></script>
	<script src="./js/tippy.min.js"></script>
	<script src="./js/jquery.tablesorter.min.js"></script>
	<script src="./js/script.js"></script>

	<!--Start of Tawk.to Script-->
	<script>
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/5cd5311d2846b90c57add080/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
	</script>
	<!--End of Tawk.to Script-->
	</body>
</html>