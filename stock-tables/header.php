<div class="stock-tables__header">
	<div class="stock-header__left">
		<form class="stock-header__search">
			<button id="submit-code"><i class="fas fa-plus"></i></button><input class="input-txt--dark" type="text" placeholder="Thêm mã CK" id="add-code">
		</form>
		<ul class="stock-header__navigation">
			<li class="has-submenu dropdown">
				<a class="nav-tab dropdown-toggle" href="#danh-muc" title="Danh mục">Danh mục</a>
				<div class="dropdown-menu--danh-muc dropdown-menu">
					<ul class="dropdown-menu__list">
						<li class="list__item active">
							<span class="list__name">
								<i class="fas fa-check-circle"></i><span>Danh mục 1</span>
							</span>
							<input class="is-hidden" type="text">
							<span class="list__buttons">
								<a class="txt-white btn--edit" href="#" title="Sửa"><i class="fas fa-edit"></i></a>
								<a class="txt-white btn--delete" href="#" title="Xóa"><i class="fas fa-times"></i></a>
							</span>
						</li>
						<li class="list__item">
							<span class="list__name">
								<i class="fas fa-check-circle"></i><span>Danh mục 2</span>
							</span>
							<input class="is-hidden" type="text">
							<span class="list__buttons">
								<a class="txt-white btn--edit" href="#" title="Sửa"><i class="fas fa-edit"></i></a>
								<a class="txt-white btn--delete" href="#" title="Xóa"><i class="fas fa-times"></i></a>
							</span>
						</li>
					</ul>
					<form class="danh-muc__tao-moi">
						<input class="input-txt--dark" type="text" placeholder="Tạo danh mục mới">
						<button type="submit"><i class="fas fa-plus"></i></button>
					</form>
				</div>
			</li>
			<li class="has-submenu"><a class="nav-tab" href="#niem-yet" title="Danh mục">Niêm yết</a></li>
			<li class="has-submenu dropdown">
				<a class="nav-tab dropdown-toggle" href="#thoa-thuan" title="Thỏa thuận">Thỏa thuận</a>
				<ul class="dropdown-menu dropdown-menu--nganh">
					<li><a href="#thoa-thuan" class="has-table" title="">GDTT HOSE</a></li>
					<li><a href="#thoa-thuan" class="has-table" title="">GDTT HNX</a></li>
					<li><a href="#thoa-thuan" class="has-table" title="">GDTT UPCOM</a></li>
				</ul>
			</li>
			<li class="has-submenu dropdown">
				<a class="nav-tab dropdown-toggle" href="#nganh" title="Ngành">Ngành</a>
				<ul class="dropdown-menu dropdown-menu--nganh">
					<li><a href="#" title="">Dầu khí</a></li>
					<li><a href="#" title="">Hóa chất</a></li>
					<li><a href="#" title="">Tài nguyên cơ bản</a></li>
					<li><a href="#" title="">Xây dựng và vật liệu</a></li>
				</ul>
			</li>
			<li><a class="nav-tab has-table" href="#khuyen-nghi" title="Khuyến nghị">CP khuyến nghị</a></li>
			<li class="has-submenu dropdown">
				<a class="nav-tab dropdown-toggle" href="#loc" title="Lọc">Lọc</a>
				<div class="dropdown-menu--loc dropdown-menu">
					<ul class="dropdown-menu__list">
						<li class="list__item active">
							<span class="list__name txt-white"><span>CP tăng trưởng</span></span>
						</li>
						<li class="list__item">
							<span class="list__name txt-white"><span>CP lợi nhuận</span></span>
						</li>
						<li class="list__item">
							<span class="list__name txt-white"><span>CP giá trị</span></span>
							<input class="is-hidden" type="text">
							<span class="list__buttons">
								<a class="txt-white btn--edit" href="#" title="Sửa"><i class="fas fa-edit"></i></a>
								<a class="txt-white btn--delete" href="#" title="Xóa"><i class="fas fa-times"></i></a>
							</span>
						</li>
					</ul>
					<div class="loc__tao-moi">
						<a href="#" title="Tạo bộ lọc" class="txt-white init-filter-modal">Tạo bộ lọc</a>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<div class="stock-header__right">
		<div class="stock-header__view-mode">
			<a href="#bang-gia" class="active nav-tab has-table" title="bang-gia"><i class="fas fa-chart-line"></i>Bảng giá</a></span>
			<a href="#co-ban" class="nav-tab has-table" title="bang-gia"><i class="fas fa-chart-line"></i>Cơ bản</a>
		</div>

		<div class="stock-header__buttons">
			<button type="button" class="init-chi-so-modal">
				<i class="fas fa-cog"></i>
			</button>
			<button type="button" class="stock-header__chart-toggle">
				<i class="fas fa-angle-up"></i>
			</button>
		</div>
	</div>

	<div class="stock-header__filter-result visble-on-filter is-hidden d-flex space-between flex-wrap">
		<div class="filter-result__number">
			Tên bộ lọc: <span class="filter__name">
			(<span><strong class="txt-green">100</strong></span> mã thỏa mãn)
		</div>
		<div class="filter-result__edit visble-on-filter is-hidden txt-white">
			<a href="#">Điều khoản sử dụng</a>
			<a href="#" class="filter__create init-filter-modal">Tạo mới bộ lọc</a>
			<a href="#" class="filter__edit init-filter-modal" data-modal-edit="enable">Chỉnh sửa bộ lọc</a>
			<a href="#">Backtest</a>
			<button type="button" class="visble-on-filter is-hidden">
				<i class="fas fa-sync-alt"></i>
			</button>
		</div>
	</div>
</div>