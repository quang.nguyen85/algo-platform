<div class="table__header sticky-table-header-wrapper">
	<table class="fixed-header">
		<thead>
			<tr>
				<th rowspan="2" class="sortable">Mã CK</th>
				<th rowspan="2" class="sortable">Sàn</th>
				<th rowspan="2" class="td-col-5 sortable">Ngành</th>
				<th rowspan="2" class="td-col-2 sortable">Tổng KL <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th colspan="3" class="cell-highlight">Khớp lệnh</th>
				<th rowspan="2" class="td-col-2 sortable">Tổng KL giao dịch <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th rowspan="2" class="td-col-2 sortable">Tỉ lệ sở hữu CP quỹ <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th rowspan="2" class="td-col-2 sortable">Thời gian niêm yết <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th rowspan="2" class="td-col-2 sortable">Vốn hóa <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th rowspan="2" class="td-col-2 sortable">ROE 4 quý so với TB ngành (%)<i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th rowspan="2" class="td-col-2 sortable">ROA 4 quý so với TB ngành (%)<i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th rowspan="2" class="td-col-2 sortable">PE 4 quý gần nhất<i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			</tr>
			<tr>
				<th class="cell-highlight sortable has-toggle toggle--percent">
					<a class="toggle__left" data-toggle="percent"><i class="fa fa-caret-left"></i></a>
					<span class="hidden-first">%</span>
					<span class="">+/-</span>
					<a class="toggle__right" data-toggle="percent"><i class="fa fa-caret-right"></i></a>
				</th>
				<th class="cell-highlight sortable">Giá</th>
				<th class="cell-highlight sortable">KL</th>
			</tr>
		</thead>
	</table>
</div>