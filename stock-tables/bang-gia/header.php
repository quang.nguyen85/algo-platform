<div class="table__header sticky-table-header-wrapper">
	<table class="fixed-header">
		<colgroup>
			<col class="col-code">

			<col class="col-price">
			<col class="col-price">
			<col class="col-price">

			<col class="col-price">
			<col class="col-vol">
			<col class="col-price">
			<col class="col-vol">
			<col class="col-price">
			<col class="col-vol">

			<col class="col-price">
			<col class="col-diff">
			<col class="col-vol">

			<col class="col-price">
			<col class="col-vol">
			<col class="col-price">
			<col class="col-vol">
			<col class="col-price">
			<col class="col-vol">

			<col class="col-vol col-vol--lg">

			<col class="col-price">
			<col class="col-price">
			<col class="col-price toggle--gia toggle--hidden">

			<col class="col-vol">
			<col class="col-vol">
		</colgroup>
		<thead>
			<tr>
				<th data-index="0" rowspan="2" class="sortable cell-highlight">Mã CK</th>
				<th data-index="1" rowspan="2" class="cell-highlight sortable">Trần</th>
				<th data-index="2" rowspan="2" class="cell-highlight sortable">Sàn</th>
				<th data-index="3" rowspan="2" class="cell-highlight sortable">TC</th>
				<th colspan="6">Dư mua</th>
				<th colspan="3" class="cell-highlight">Khớp</th>
				<th colspan="6">Dư bán</th>
				<th data-index="19" rowspan="2" class="sortable has-toggle toggle--tong">
					<a class="toggle__left" data-toggle="tong"><i class="fa fa-caret-left"></i></a>
					<span class="hidden-first">Tổng<br>GT</span>
					<span>Tổng<br>KL</span>
					<a class="toggle__right" data-toggle="tong">
						<i class="fa fa-caret-right"></i>
					</a>
				</th>
				<th colspan="3" class="has-toggle toggle--gia cell-highlight">
					<a class="toggle__left" data-toggle="gia"><i class="fa fa-caret-left"></i></a>
					<span class="hidden-first">Dư</span>
					<span>Giá</span>
					<a class="toggle__right" data-toggle="gia"><i class="fa fa-caret-right"></i></a>
				</th>
				<th colspan="2" class="has-toggle toggle--dtnn toggle-col-span">
					<a class="toggle__left" data-toggle="dtnn"><i class="fa fa-caret-left"></i></a>
					<span>NĐTNN</span>
					<a class="toggle__right" data-toggle="dtnn"><i class="fa fa-caret-right"></i></a>
				</th>
			</tr>
			<tr>
				<th data-index="4" class="sortable">Giá 3</th>
				<th data-index="5" class="sortable headerSortDown">KL 3</th>
				<th data-index="6" class="sortable">Giá 2</th>
				<th data-index="7" class="sortable">KL 2</th>
				<th data-index="8" class="sortable">Giá 1</th>
				<th data-index="9" class="sortable">KL 1</th>
				<th data-index="11" class="cell-highlight sortable">Giá</th>
				<th data-index="10" class="cell-highlight sortable has-toggle toggle--percent">
					<a class="toggle__left" data-toggle="percent"><i class="fa fa-caret-left"></i></a>
					<span class="hidden-first">%</span>
					<span class="">+/-</span>
					<a class="toggle__right" data-toggle="percent"><i class="fa fa-caret-right"></i></a>
				</th>
				<th data-index="12" class="cell-highlight sortable">KL</th>
				<th data-index="13" class="sortable">Giá 1</th>
				<th data-index="14" class="sortable">KL 1</th>
				<th data-index="15" class="sortable">Giá 2</th>
				<th data-index="16" class="sortable">KL 2</th>
				<th data-index="17" class="sortable">Giá 3</th>
				<th data-index="18" class="sortable">KL 3</th>
				<th class="cell-highlight toggle--gia">
					<span data-index="20" class="sortable hidden-first">Mua</span>
					<span data-index="20" class="sortable">Cao</span>
				</th>
				<th class="cell-highlight toggle--gia">
					<span data-index="21" class="sortable hidden-first">Bán</span>
					<span data-index="21" class="sortable">Thấp</span>
				</th>
				<th class="cell-highlight toggle--gia toggle--hidden">
					<span data-index="22" class="sortable">TB</span>
				</th>
				<th class="toggle--dtnn">
					<span data-index="23" class="sortable hidden-first">Room</span>
					<span data-index="23" class="sortable">Mua</span>
				</th>
				<th class="toggle--dtnn">
					<span data-index="24" class="sortable hidden-first">%</span>
					<span data-index="24" class="sortable">Bán</span>
				</th>
			</tr>
		</thead>
	</table>
</div>