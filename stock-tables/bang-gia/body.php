<table class="table__body">
	<colgroup>
		<col class="col-code">

		<col class="col-price">
		<col class="col-price">
		<col class="col-price">

		<col class="col-price">
		<col class="col-vol">
		<col class="col-price">
		<col class="col-vol">
		<col class="col-price">
		<col class="col-vol">

		<col class="col-price">
		<col class="col-diff">
		<col class="col-vol">

		<col class="col-price">
		<col class="col-vol">
		<col class="col-price">
		<col class="col-vol">
		<col class="col-price">
		<col class="col-vol">

		<col class="col-vol col-vol--lg">

		<col class="col-price">
		<col class="col-price">
		<col class="col-price toggle--gia toggle--hidden">

		<col class="col-vol">
		<col class="col-vol">
	</colgroup>
	<thead class="is-hidden">
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
	</thead>
	<tbody>
		<?php for( $i = 0; $i < 20; $i++ ) : ?>
			<tr data-id="row-a-<?= $i ?>">
				<td class="tooltip txt-ma txt-ma--sm txt-green cell-highlight" data-tippy-content="Công ty CP Cơ điện Hà Nội 1">
					<span class="code-wrap"><a href="./chi-tiet-ma.php" target="_blank">E1VFVN30</a><i class="fas fa-times remove"></i></span>
				</td>
				<td class="txt-pink cell-highlight">60.30</td>
				<td class="txt-cyan cell-highlight">52.50</td>
				<td class="txt-yellow cell-highlight">56.40</td>
				<td class="txt-green">56.60</td>
				<td class="txt-green">2.15</td>
				<td class="txt-green">56.70</td>
				<td class="txt-green">3.71</td>
				<td class="txt-green">56.80</td>
				<td class="txt-green">7.21</td>
				<td class="txt-green cell-highlight">56.90</td>
				<td class="txt-green cell-highlight toggle--percent">
					<span class="hidden-first">10%</span>
					<span>0.50</span>
				</td>
				<td class="txt-green cell-highlight">52</td>
				<td class="txt-green">3.62</td>
				<td class="txt-green">57.00</td>
				<td class="txt-green">1.65</td>
				<td class="txt-green">57.10</td>
				<td class="txt-green">1.65</td>
				<td class="txt-green">3.95</td>
				<td class="txt-white toggle--tong">
					<span class="hidden-first">140</span>
					<span>130.12</span>
				</td>
				<td class="txt-green cell-highlight toggle--gia">
					<span class="hidden-first">53.54</span>
					<span>57.20</span>
				</td>
				<td class="txt-green cell-highlight toggle--gia">
					<span class="hidden-first">51.14</span>
					<span>56.90</span>
				</td>
				<td class="txt-green cell-highlight toggle--gia toggle--hidden">
					<span class="hidden-first">54.75</span>
					<span>56.70</span>
				</td>
				<td class="txt-green toggle--dtnn">
					<span class="hidden-first">2.49</span>
					<span>1.60</span>
				</td>
				<td class="txt-green toggle--dtnn">
					<span class="hidden-first">1.25%</span>
					<span>15.60</span>
				</td>
			</tr>
			<tr data-id="row-b-<?= $i ?>">
				<td class="tooltip txt-ma txt-red cell-highlight" data-tippy-content="Ngân hàng thương mại cổ phần công thương Việt Nam">
					<span class="code-wrap">
						<a href="./chi-tiet-ma.php" target="_blank">CTG</a><?= $i ?><?php if ( 5 === $i ) : ?><i class="fas fa-star"></i><?php endif; ?><i class="fas fa-times remove"></i>
					</span>
				</td>
				<td class="txt-pink cell-highlight">22.45</td>
				<td class="txt-cyan cell-highlight">19.55</td>
				<td class="txt-yellow cell-highlight">21.00</td>
				<td class="txt-red">20.80</td>
				<td class="txt-red">90.84</td>
				<td class="txt-red">20.85</td>
				<td class="txt-red">91.12</td>
				<td class="txt-red">20.90</td>
				<td class="txt-red">72.21</td>
				<td class="txt-red cell-highlight">20.95</td>
				<td class="txt-red cell-highlight toggle--percent">
					<span class="hidden-first">-10%</span>
					<span>-0.50</span>
				</td>
				<td class="txt-red cell-highlight">15.60</td>
				<td class="txt-red">20.95</td>
				<td class="txt-red">1.65</td>
				<td class="txt-yellow">21.00</td>
				<td class="txt-yellow">9.65</td>
				<td class="txt-green">21.05</td>
				<td class="txt-green">91.20</td>
				<td class="txt-white toggle--tong">
					<span class="hidden-first">100</span>
					<span>91.20</span>
				</td>
				<td class="txt-green cell-highlight toggle--gia">
					<span class="hidden-first">19.50</span>
					<span>21.25</span>
				</td>
				<td class="txt-green cell-highlight toggle--gia">
					<span class="hidden-first">20.15</span>
					<span>21.00</span>
				</td>
				<td class="txt-green cell-highlight toggle--gia toggle--hidden">
					<span class="hidden-first">18.75</span>
					<span>20.90</span>
				</td>
				<td class="txt-green toggle--dtnn">
					<span class="hidden-first">3.14</span>
					<span>2.62</span>
				</td>
				<td class="txt-green toggle--dtnn">
					<span class="hidden-first">2.24%</span>
					<span>32.12</span>
				</td>
			</tr>
		<?php endfor; ?>
	</tbody>
</table>