<table class="table__body">
	<colgroup>
		<col>
		<col>
		<col class="col-nganh">
		<col class="col-kl">
		<col class="col-10-phien">
		<col class="col-gt">
		<col>
		<col>
		<col>
		<col>
		<col>
		<col>
		<col>
		<col>
		<col class="col-eps">
		<col>
		<col>
		<col class="col-nn">
		<col class="col-nn">
	</colgroup>
	<thead class="is-hidden">
		<tr>
			<th rowspan="2">Mã CK</th>
			<th rowspan="2">Sàn</th>
			<th rowspan="2" class="sortable">Ngành</th>
			<th rowspan="2" class="td-col-2 sortable">Tổng KL <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			<th rowspan="2" class="sortable">%KL / KLTB<br>10 phiên <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			<th rowspan="2" class="td-col-2 sortable">Tổng GT <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			<th rowspan="2" class="sortable cell-highlight">TC</th>
			<th rowspan="2" class="sortable cell-highlight">Khớp lệnh</th>
			<th rowspan="2" class="sortable cell-highlight">+/-</th>
			<th rowspan="2" class="sortable cell-highlight">%</th>
			<th rowspan="2" class="td-col-2 sortable">Vốn hóa</th>
			<th rowspan="2">PE <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			<th rowspan="2">PB <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			<th rowspan="2">PS <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			<th rowspan="2">EPS<br>4 quý <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			<th rowspan="2">ROE <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			<th rowspan="2">ROA <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			<th colspan="2">NĐTNN <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
		</tr>
		<tr>
			<th>Mua</th>
			<th>Bán</th>
		</tr>
	</thead>
	<tbody>
		<?php for ( $i = 0; $i < 20; $i++ ) : ?>
			<tr data-id="row-a-<?= $i ?>">
				<td class="tooltip txt-ma txt-yellow txt-left" data-tippy-content="Công ty CP Cơ điện Hà Nội 1"><a href="./chi-tiet-ma.php" target="_blank">NVL</a><?= $i ?></td>
				<td class="txt-white txt-left">HOSE</td>
				<td class="txt-white txt-left">Xây dựng và vật liệu</td>
				<td class="txt-white">1.352.62</td>
				<td class="txt-white">32.15%</td>
				<td class="txt-white">52.326</td>
				<td class="txt-yellow cell-highlight">56.90</td>
				<td class="txt-green cell-highlight">57.90</td>
				<td class="txt-green cell-highlight">2.10</td>
				<td class="txt-green cell-highlight">3.23%</td>
				<td class="txt-white td-col-2">1.202.326</td>
				<td class="txt-white">12.1</td>
				<td class="txt-white">1.3</td>
				<td class="txt-white">2.1</td>
				<td class="txt-white">1.355</td>
				<td class="txt-white">12.23%</td>
				<td class="txt-white">2.01%</td>
				<td class="txt-white">15.60</td>
				<td class="txt-white">26.21</td>
			</tr>
			<tr data-id="row-b-<?= $i ?>">
				<td class="tooltip txt-ma txt-red txt-left" data-tippy-content="Ngân hàng thương mại cổ phần công thương Việt Nam"><a href="./chi-tiet-ma.php" target="_blank">CTG</a><?= $i ?></td>
				<td class="txt-white txt-left">HOSE</td>
				<td class="txt-white txt-left">Bất động sản</td>
				<td class="txt-white">321.23</td>
				<td class="txt-white">128.32%</td>
				<td class="txt-white">31.125</td>
				<td class="txt-yellow cell-highlight">20.95</td>
				<td class="txt-red cell-highlight">21.95</td>
				<td class="txt-red cell-highlight">-1.65</td>
				<td class="txt-red cell-highlight">-3.60%</td>
				<td class="txt-white td-col-2">32.321.123</td>
				<td class="txt-white">9.1</td>
				<td class="txt-white">1.2</td>
				<td class="txt-white">1.9</td>
				<td class="txt-white">3.215</td>
				<td class="txt-white">15.32%</td>
				<td class="txt-white">3.15%</td>
				<td class="txt-white">32.12</td>
				<td class="txt-white">210.21</td>
			</tr>
		<?php endfor; ?>
	</tbody>
</table>
