<div class="table__header sticky-table-header-wrapper">
	<table class="fixed-header">
		<colgroup>
			<col>
			<col>
			<col class="col-nganh">
			<col class="col-kl">
			<col class="col-10-phien">
			<col class="col-gt">
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col class="col-eps">
			<col>
			<col>
			<col class="col-nn">
			<col class="col-nn">
		</colgroup>
		<thead>
			<tr>
				<th data-index="0" data-index="0" rowspan="2" class="sortable">Mã CK</th>
				<th data-index="1" rowspan="2" class="sortable">Sàn</th>
				<th data-index="2" rowspan="2" class="sortable">Ngành</th>
				<th data-index="3" rowspan="2" class="sortable">Tổng KL <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th data-index="4" rowspan="2" class="sortable">%KL / KLTB<br>10 phiên <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th data-index="5" rowspan="2" class="sortable">Tổng GT <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th data-index="6" rowspan="2" class="sortable cell-highlight">TC</th>
				<th data-index="6" rowspan="2" class="sortable cell-highlight">Khớp lệnh</th>
				<th data-index="7" rowspan="2" class="sortable cell-highlight">+/-</th>
				<th data-index="8" rowspan="2" class="sortable cell-highlight">%</th>
				<th data-index="9" rowspan="2" class="td-col-2 sortable">Vốn hóa</th>
				<th data-index="10" rowspan="2" class="sortable">PE <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th data-index="11" rowspan="2" class="sortable">PB <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th data-index="12" rowspan="2" class="sortable">PS <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th data-index="13" rowspan="2" class="sortable">EPS<br>4 quý <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th data-index="14" rowspan="2" class="sortable">ROE <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th data-index="15" rowspan="2" class="sortable">ROA <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
				<th colspan="2">NĐTNN <i class="tooltip fas fa-info-circle" data-tippy-content="Thông tin chi tiết giải thích về chỉ số"></i></th>
			</tr>
			<tr>
				<th data-index="16" class="sortable">Mua</th>
				<th data-index="17" class="sortable">Bán</th>
			</tr>
		</thead>
	</table>
</div>