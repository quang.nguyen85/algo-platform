<div class="table__header sticky-table-header-wrapper">
	<table class="fixed-header">
		<tr>
			<th rowspan="2" class="ma-cq">Mã CQ</th>
			<th rowspan="2" class="ngay-dh">Ngày ĐH</th>
			<th rowspan="2" class="cell-highlight">Trần</th>
			<th rowspan="2" class="cell-highlight">Sàn</th>
			<th rowspan="2" class="cell-highlight">TC</th>
			<th colspan="6">Dư mua</th>
			<th colspan="3" class="cell-highlight">Khớp lệnh</th>
			<th colspan="6">Dư bán</th>
			<th rowspan="2" class="tong-kl">Tổng KL</th>
			<th colspan="3" class="has-toggle toggle--gia cell-highlight">
				<a class="toggle__left" data-toggle="gia"><i class="fa fa-caret-left"></i></a>
				<span class="hidden-first">Dư</span>
				<span>Giá</span>
				<a class="toggle__right" data-toggle="gia"><i class="fa fa-caret-right"></i></a>
			</th>
			<th colspan="2">NN</th>
		</tr>
		<tr>
			<th>Giá 3</th>
			<th>KL 3</th>
			<th>Giá 2</th>
			<th>KL 2</th>
			<th>Giá 1</th>
			<th>KL 1</th>
			<th class="cell-highlight">Giá</th>
			<th class="cell-highlight has-toggle toggle--percent">
				<a class="toggle__left" data-toggle="percent"><i class="fa fa-caret-left"></i></a>
				<span class="hidden-first">%</span>
				<span class="">+/-</span>
				<a class="toggle__right" data-toggle="percent"><i class="fa fa-caret-right"></i></a>
			</th>
			<th class="cell-highlight">KL</th>
			<th>Giá 1</th>
			<th>KL 1</th>
			<th>Giá 2</th>
			<th>KL 2</th>
			<th>Giá 3</th>
			<th>KL 3</th>
			<th class="cell-highlight toggle--gia">
				<span class="hidden-first">Mua</span>
				<span>Cao</span>
			</th>
			<th class="cell-highlight toggle--gia">
				<span class="hidden-first">Bán</span>
				<span>Thấp</span>
			</th>
			<th class="cell-highlight toggle--gia toggle--hidden">
				<span>TB</span>
			</th>
			<th>Mua</th>
			<th>Bán</th>
		</tr>
	</table>
</div>