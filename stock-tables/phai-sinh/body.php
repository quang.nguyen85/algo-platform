<table class="table__body">
	<tr class="item">
		<td class="txt-ma txt-green ma-hd">VN30F1905</td>
		<td class="ngay-dh">19/05/19</td>
		<td class="txt-pink cell-highlight">60.00</td>
		<td class="txt-cyan cell-highlight">52.50</td>
		<td class="txt-yellow cell-highlight">56.40</td>
		<td class="txt-green">56.60</td>
		<td class="txt-green">2.15</td>
		<td class="txt-green">56.70</td>
		<td class="txt-green">3.71</td>
		<td class="txt-green">56.80</td>
		<td class="txt-green">7.21</td>
		<td class="txt-green cell-highlight">56.90</td>
		<td class="txt-green cell-highlight toggle--percent">
			<span class="hidden-first">10%</span>
			<span>0.50</span>
		</td>
		<td class="txt-green cell-highlight">52</td>
		<td class="txt-green cell-highlight">0.50</td>
		<td class="txt-green">57.00</td>
		<td class="txt-green">3.62</td>
		<td class="txt-green">57.10</td>
		<td class="txt-green">1.65</td>
		<td class="txt-green">57.20</td>
		<td class="txt-green">1.65</td>
		<td class="txt-white tong-kl">130.12</td>
		<td class="txt-white">731</td>

		<td class="txt-green cell-highlight toggle--gia">
			<span class="hidden-first">53.54</span>
			<span>57.20</span>
		</td>
		<td class="txt-green cell-highlight toggle--gia">
			<span class="hidden-first">51.14</span>
			<span>56.90</span>
		</td>
		<td class="txt-green cell-highlight toggle--gia toggle--hidden">
			<span class="hidden-first">54.75</span>
			<span>56.70</span>
		</td>
		<td>1.60</td>
		<td>15.60</td>
	</tr>
	<tr class="item-info">
		<td colspan="28">
			<div class="item-info__wrap">
				<div class="item-info__content">
					<div class="item-info__tabs">
						<a href="#giao-dich-trong-ngay" class="is-active">Giao dịch trong ngày</a>
						<a href="#lich-su-giao-dich">Lịch sử giao dịch</a>
						<a href="#phan-tich-do-lech">Phân tích độ lệch</a>
						<a href="#anh-huong-vn30">Ảnh hưởng VN30</a>
						<a href="./phai-sinh.php" target="_blank">Phân tích kỹ thuật</a>
					</div>
					<div class="item-info__tab is-active" data-tab="#giao-dich-trong-ngay">
						<div class="item-info__section">
							<img src="./images/phai-sinh/1.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/2.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/3.svg">
						</div>
					</div>
					<div class="item-info__tab" data-tab="#lich-su-giao-dich">
						<div class="item-info__section">
							<img src="./images/phai-sinh/4.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/5.svg">
						</div>
						<div class="item-info__section item-info__section--no-padding">
							<table>
								<tr>
									<th rowspan="2">Ngày</th>
									<th rowspan="2">Đóng cửa</th>
									<th rowspan="2">Thay đổi giá</th>
									<th rowspan="2">Giá tham chiếu</th>
									<th rowspan="2">NN<br>mua bán ròng</th>
									<th colspan="2">Khớp lệnh</th>
								</tr>
								<tr>
									<th>KLGD</th>
									<th>GTGD</th>
								</tr>
								<?php for ( $i = 1; $i <= 5; $i++ ) : ?>
									<tr>
										<td class="txt-green">26/4/2019</td>
										<td class="txt-green">877</td>
										<td class="txt-green">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
										<td class="txt-green">856</td>
										<td class="txt-green">125</td>
										<td class="txt-green">101,200</td>
										<td class="txt-green">1,500,350,000</td>
									</tr>
									<tr>
										<td class="txt-red">26/4/2019</td>
										<td class="txt-red">877</td>
										<td class="txt-red">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
										<td class="txt-red">856</td>
										<td class="txt-red">125</td>
										<td class="txt-red">101,200</td>
										<td class="txt-red">1,500,350,000</td>
									</tr>
								<?php endfor; ?>
							</table>
						</div>
					</div>
					<div class="item-info__tab" data-tab="#phan-tich-do-lech">
						<div class="item-info__section">
							<img src="./images/phai-sinh/6.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/7.svg">
						</div>
					</div>
					<div class="item-info__tab" data-tab="#anh-huong-vn30">
						<div class="item-info__section">
							<img src="./images/phai-sinh/8.svg">
						</div>
					</div>
				</div>
				<div class="item-info__sidebar">
					<div class="item-info__sidebar__nav">
						<a href="#khop-lenh" class="is-active">Khớp lệnh</a>
						<a href="#buoc-gia">Bước giá</a>
					</div>
					<div class="item-info__sidebar__tab is-active" data-tab="#khop-lenh">
						<table>
							<tr>
								<th>Thời gian</th>
								<th>Giá</th>
								<th>KL</th>
								<th>Tổng KL</th>
							</tr>
							<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
								<tr>
									<td class="txt-green txt-right">15:05:20</td>
									<td class="txt-green txt-right">95.00</td>
									<td class="txt-green txt-right">39</td>
									<td class="txt-green txt-right">153</td>
								</tr>
							<?php endfor; ?>
						</table>
					</div>
					<div class="item-info__sidebar__tab" data-tab="#buoc-gia">
						<table>
							<tr>
								<th colspan="2">Dư mua</th>
								<th colspan="2">Dư bán</th>
							</tr>
							<tr>
								<th>Giá</th>
								<th>KL</th>
								<th>KL</th>
								<th>Giá</th>
							</tr>
							<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
								<tr>
									<td class="txt-green txt-right">85.75</td>
									<td class="txt-green txt-right">35</td>
									<td class="txt-green txt-right">87.50</td>
									<td class="txt-green txt-right">36</td>
								</tr>
							<?php endfor; ?>
							<tr>
								<td class="txt-center code-details__tables__total" colspan="2">350</td>
								<td class="txt-center code-details__tables__total" colspan="2">360</td>
							</tr>
						</table>
						<div class="item-info__sidebar__stats">
							<div class="bg-green-2" style="width: 23%">23%</div>
							<div class="bg-red" style="width: 77%">77%</div>
						</div>
					</div>
				</div>
			</div>
		</td>
	</tr>

	<tr class="item">
		<td class="txt-ma txt-green ma-hd">VN30F1906</td>
		<td class="ngay-dh">19/05/19</td>
		<td class="txt-pink cell-highlight">60.00</td>
		<td class="txt-cyan cell-highlight">52.50</td>
		<td class="txt-yellow cell-highlight">56.40</td>
		<td class="txt-green">56.60</td>
		<td class="txt-green">2.15</td>
		<td class="txt-green">56.70</td>
		<td class="txt-green">3.71</td>
		<td class="txt-green">56.80</td>
		<td class="txt-green">7.21</td>
		<td class="txt-green cell-highlight">56.90</td>
		<td class="txt-green cell-highlight toggle--percent">
			<span class="hidden-first">10%</span>
			<span>0.50</span>
		</td>
		<td class="txt-green cell-highlight">52</td>
		<td class="txt-green cell-highlight">0.50</td>
		<td class="txt-green">57.00</td>
		<td class="txt-green">3.62</td>
		<td class="txt-green">57.10</td>
		<td class="txt-green">1.65</td>
		<td class="txt-green">57.20</td>
		<td class="txt-green">1.65</td>
		<td class="txt-white tong-kl">130.12</td>
		<td class="txt-white">731</td>

		<td class="txt-green cell-highlight toggle--gia">
			<span class="hidden-first">53.54</span>
			<span>57.20</span>
		</td>
		<td class="txt-green cell-highlight toggle--gia">
			<span class="hidden-first">51.14</span>
			<span>56.90</span>
		</td>
		<td class="txt-green cell-highlight toggle--gia toggle--hidden">
			<span class="hidden-first">54.75</span>
			<span>56.70</span>
		</td>
		<td>1.60</td>
		<td>15.60</td>
	</tr>
	<tr class="item-info">
		<td colspan="28">
			<div class="item-info__wrap">
				<div class="item-info__content">
					<div class="item-info__tabs">
						<a href="#giao-dich-trong-ngay" class="is-active">Giao dịch trong ngày</a>
						<a href="#lich-su-giao-dich">Lịch sử giao dịch</a>
						<a href="#phan-tich-do-lech">Phân tích độ lệch</a>
						<a href="#anh-huong-vn30">Ảnh hưởng VN30</a>
						<a href="./phai-sinh.php" target="_blank">Phân tích kỹ thuật</a>
					</div>
					<div class="item-info__tab is-active" data-tab="#giao-dich-trong-ngay">
						<div class="item-info__section">
							<img src="./images/phai-sinh/1.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/2.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/3.svg">
						</div>
					</div>
					<div class="item-info__tab" data-tab="#lich-su-giao-dich">
						<div class="item-info__section">
							<img src="./images/phai-sinh/4.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/5.svg">
						</div>
						<div class="item-info__section item-info__section--no-padding">
							<table>
								<tr>
									<th rowspan="2">Ngày</th>
									<th rowspan="2">Đóng cửa</th>
									<th rowspan="2">Thay đổi giá</th>
									<th rowspan="2">Giá tham chiếu</th>
									<th rowspan="2">NN<br>mua bán ròng</th>
									<th colspan="2">Khớp lệnh</th>
								</tr>
								<tr>
									<th>KLGD</th>
									<th>GTGD</th>
								</tr>
								<?php for ( $i = 1; $i <= 5; $i++ ) : ?>
									<tr>
										<td class="txt-green">26/4/2019</td>
										<td class="txt-green">877</td>
										<td class="txt-green">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
										<td class="txt-green">856</td>
										<td class="txt-green">125</td>
										<td class="txt-green">101,200</td>
										<td class="txt-green">1,500,350,000</td>
									</tr>
									<tr>
										<td class="txt-red">26/4/2019</td>
										<td class="txt-red">877</td>
										<td class="txt-red">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
										<td class="txt-red">856</td>
										<td class="txt-red">125</td>
										<td class="txt-red">101,200</td>
										<td class="txt-red">1,500,350,000</td>
									</tr>
								<?php endfor; ?>
							</table>
						</div>
					</div>
					<div class="item-info__tab" data-tab="#phan-tich-do-lech">
						<div class="item-info__section">
							<img src="./images/phai-sinh/6.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/7.svg">
						</div>
					</div>
					<div class="item-info__tab" data-tab="#anh-huong-vn30">
						<div class="item-info__section">
							<img src="./images/phai-sinh/8.svg">
						</div>
					</div>
				</div>
				<div class="item-info__sidebar">
					<div class="item-info__sidebar__nav">
						<a href="#khop-lenh" class="is-active">Khớp lệnh</a>
						<a href="#buoc-gia">Bước giá</a>
					</div>
					<div class="item-info__sidebar__tab is-active" data-tab="#khop-lenh">
						<table>
							<tr>
								<th>Thời gian</th>
								<th>Giá</th>
								<th>KL</th>
								<th>Tổng KL</th>
							</tr>
							<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
								<tr>
									<td class="txt-green txt-right">15:05:20</td>
									<td class="txt-green txt-right">95.00</td>
									<td class="txt-green txt-right">39</td>
									<td class="txt-green txt-right">153</td>
								</tr>
							<?php endfor; ?>
						</table>
					</div>
					<div class="item-info__sidebar__tab" data-tab="#buoc-gia">
						<table>
							<tr>
								<th colspan="2">Dư mua</th>
								<th colspan="2">Dư bán</th>
							</tr>
							<tr>
								<th>Giá</th>
								<th>KL</th>
								<th>Giá</th>
								<th>KL</th>
							</tr>
							<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
								<tr>
									<td class="txt-green txt-right">85.75</td>
									<td class="txt-green txt-right">35</td>
									<td class="txt-green txt-right">87.50</td>
									<td class="txt-green txt-right">36</td>
								</tr>
							<?php endfor; ?>
						</table>
						<div class="item-info__sidebar__stats">
							<div class="bg-green" style="width: 23%">23%</div>
							<div class="bg-red" style="width: 77%">77%</div>
						</div>
					</div>
				</div>
			</div>
		</td>
	</tr>

	<tr class="item">
		<td class="txt-ma txt-green ma-hd">VN30F1909</td>
		<td class="ngay-dh">19/05/19</td>
		<td class="txt-pink cell-highlight">60.00</td>
		<td class="txt-cyan cell-highlight">52.50</td>
		<td class="txt-yellow cell-highlight">56.40</td>
		<td class="txt-green">56.60</td>
		<td class="txt-green">2.15</td>
		<td class="txt-green">56.70</td>
		<td class="txt-green">3.71</td>
		<td class="txt-green">56.80</td>
		<td class="txt-green">7.21</td>
		<td class="txt-green cell-highlight">56.90</td>
		<td class="txt-green cell-highlight toggle--percent">
			<span class="hidden-first">10%</span>
			<span>0.50</span>
		</td>
		<td class="txt-green cell-highlight">52</td>
		<td class="txt-green cell-highlight">0.50</td>
		<td class="txt-green">57.00</td>
		<td class="txt-green">3.62</td>
		<td class="txt-green">57.10</td>
		<td class="txt-green">1.65</td>
		<td class="txt-green">57.20</td>
		<td class="txt-green">1.65</td>
		<td class="txt-white tong-kl">130.12</td>
		<td class="txt-white">731</td>

		<td class="txt-green cell-highlight toggle--gia">
			<span class="hidden-first">53.54</span>
			<span>57.20</span>
		</td>
		<td class="txt-green cell-highlight toggle--gia">
			<span class="hidden-first">51.14</span>
			<span>56.90</span>
		</td>
		<td class="txt-green cell-highlight toggle--gia toggle--hidden">
			<span class="hidden-first">54.75</span>
			<span>56.70</span>
		</td>
		<td>1.60</td>
		<td>15.60</td>
	</tr>
	<tr class="item-info">
		<td colspan="28">
			<div class="item-info__wrap">
				<div class="item-info__content">
					<div class="item-info__tabs">
						<a href="#giao-dich-trong-ngay" class="is-active">Giao dịch trong ngày</a>
						<a href="#lich-su-giao-dich">Lịch sử giao dịch</a>
						<a href="#phan-tich-do-lech">Phân tích độ lệch</a>
						<a href="#anh-huong-vn30">Ảnh hưởng VN30</a>
						<a href="./phai-sinh.php" target="_blank">Phân tích kỹ thuật</a>
					</div>
					<div class="item-info__tab is-active" data-tab="#giao-dich-trong-ngay">
						<div class="item-info__section">
							<img src="./images/phai-sinh/1.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/2.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/3.svg">
						</div>
					</div>
					<div class="item-info__tab" data-tab="#lich-su-giao-dich">
						<div class="item-info__section">
							<img src="./images/phai-sinh/4.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/5.svg">
						</div>
						<div class="item-info__section item-info__section--no-padding">
							<table>
								<tr>
									<th rowspan="2">Ngày</th>
									<th rowspan="2">Đóng cửa</th>
									<th rowspan="2">Thay đổi giá</th>
									<th rowspan="2">Giá tham chiếu</th>
									<th rowspan="2">NN<br>mua bán ròng</th>
									<th colspan="2">Khớp lệnh</th>
								</tr>
								<tr>
									<th>KLGD</th>
									<th>GTGD</th>
								</tr>
								<?php for ( $i = 1; $i <= 5; $i++ ) : ?>
									<tr>
										<td class="txt-green">26/4/2019</td>
										<td class="txt-green">877</td>
										<td class="txt-green">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
										<td class="txt-green">856</td>
										<td class="txt-green">125</td>
										<td class="txt-green">101,200</td>
										<td class="txt-green">1,500,350,000</td>
									</tr>
									<tr>
										<td class="txt-red">26/4/2019</td>
										<td class="txt-red">877</td>
										<td class="txt-red">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
										<td class="txt-red">856</td>
										<td class="txt-red">125</td>
										<td class="txt-red">101,200</td>
										<td class="txt-red">1,500,350,000</td>
									</tr>
								<?php endfor; ?>
							</table>
						</div>
					</div>
					<div class="item-info__tab" data-tab="#phan-tich-do-lech">
						<div class="item-info__section">
							<img src="./images/phai-sinh/6.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/7.svg">
						</div>
					</div>
					<div class="item-info__tab" data-tab="#anh-huong-vn30">
						<div class="item-info__section">
							<img src="./images/phai-sinh/8.svg">
						</div>
					</div>
				</div>
				<div class="item-info__sidebar">
					<div class="item-info__sidebar__nav">
						<a href="#khop-lenh" class="is-active">Khớp lệnh</a>
						<a href="#buoc-gia">Bước giá</a>
					</div>
					<div class="item-info__sidebar__tab is-active" data-tab="#khop-lenh">
						<table>
							<tr>
								<th>Thời gian</th>
								<th>Giá</th>
								<th>KL</th>
								<th>Tổng KL</th>
							</tr>
							<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
								<tr>
									<td class="txt-green txt-right">15:05:20</td>
									<td class="txt-green txt-right">95.00</td>
									<td class="txt-green txt-right">39</td>
									<td class="txt-green txt-right">153</td>
								</tr>
							<?php endfor; ?>
						</table>
					</div>
					<div class="item-info__sidebar__tab" data-tab="#buoc-gia">
						<table>
							<tr>
								<th colspan="2">Dư mua</th>
								<th colspan="2">Dư bán</th>
							</tr>
							<tr>
								<th>Giá</th>
								<th>KL</th>
								<th>Giá</th>
								<th>KL</th>
							</tr>
							<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
								<tr>
									<td class="txt-green txt-right">85.75</td>
									<td class="txt-green txt-right">35</td>
									<td class="txt-green txt-right">87.50</td>
									<td class="txt-green txt-right">36</td>
								</tr>
							<?php endfor; ?>
						</table>
						<div class="item-info__sidebar__stats">
							<div class="bg-green" style="width: 23%">23%</div>
							<div class="bg-red" style="width: 77%">77%</div>
						</div>
					</div>
				</div>
			</div>
		</td>
	</tr>

	<tr class="item">
		<td class="txt-ma txt-green ma-hd">VN30F1912</td>
		<td class="ngay-dh">19/05/19</td>
		<td class="txt-pink cell-highlight">60.00</td>
		<td class="txt-cyan cell-highlight">52.50</td>
		<td class="txt-yellow cell-highlight">56.40</td>
		<td class="txt-green">56.60</td>
		<td class="txt-green">2.15</td>
		<td class="txt-green">56.70</td>
		<td class="txt-green">3.71</td>
		<td class="txt-green">56.80</td>
		<td class="txt-green">7.21</td>
		<td class="txt-green cell-highlight">56.90</td>
		<td class="txt-green cell-highlight toggle--percent">
			<span class="hidden-first">10%</span>
			<span>0.50</span>
		</td>
		<td class="txt-green cell-highlight">52</td>
		<td class="txt-green cell-highlight">0.50</td>
		<td class="txt-green">57.00</td>
		<td class="txt-green">3.62</td>
		<td class="txt-green">57.10</td>
		<td class="txt-green">1.65</td>
		<td class="txt-green">57.20</td>
		<td class="txt-green">1.65</td>
		<td class="txt-white tong-kl">130.12</td>
		<td class="txt-white">731</td>

		<td class="txt-green cell-highlight toggle--gia">
			<span class="hidden-first">53.54</span>
			<span>57.20</span>
		</td>
		<td class="txt-green cell-highlight toggle--gia">
			<span class="hidden-first">51.14</span>
			<span>56.90</span>
		</td>
		<td class="txt-green cell-highlight toggle--gia toggle--hidden">
			<span class="hidden-first">54.75</span>
			<span>56.70</span>
		</td>
		<td>1.60</td>
		<td>15.60</td>
	</tr>
	<tr class="item-info">
		<td colspan="28">
			<div class="item-info__wrap">
				<div class="item-info__content">
					<div class="item-info__tabs">
						<a href="#giao-dich-trong-ngay" class="is-active">Giao dịch trong ngày</a>
						<a href="#lich-su-giao-dich">Lịch sử giao dịch</a>
						<a href="#phan-tich-do-lech">Phân tích độ lệch</a>
						<a href="#anh-huong-vn30">Ảnh hưởng VN30</a>
						<a href="./phai-sinh.php" target="_blank">Phân tích kỹ thuật</a>
					</div>
					<div class="item-info__tab is-active" data-tab="#giao-dich-trong-ngay">
						<div class="item-info__section">
							<img src="./images/phai-sinh/1.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/2.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/3.svg">
						</div>
					</div>
					<div class="item-info__tab" data-tab="#lich-su-giao-dich">
						<div class="item-info__section">
							<img src="./images/phai-sinh/4.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/5.svg">
						</div>
						<div class="item-info__section item-info__section--no-padding">
							<table>
								<tr>
									<th rowspan="2">Ngày</th>
									<th rowspan="2">Đóng cửa</th>
									<th rowspan="2">Thay đổi giá</th>
									<th rowspan="2">Giá tham chiếu</th>
									<th rowspan="2">NN<br>mua bán ròng</th>
									<th colspan="2">Khớp lệnh</th>
								</tr>
								<tr>
									<th>KLGD</th>
									<th>GTGD</th>
								</tr>
								<?php for ( $i = 1; $i <= 5; $i++ ) : ?>
									<tr>
										<td class="txt-green">26/4/2019</td>
										<td class="txt-green">877</td>
										<td class="txt-green">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
										<td class="txt-green">856</td>
										<td class="txt-green">125</td>
										<td class="txt-green">101,200</td>
										<td class="txt-green">1,500,350,000</td>
									</tr>
									<tr>
										<td class="txt-red">26/4/2019</td>
										<td class="txt-red">877</td>
										<td class="txt-red">8.40 (0.97%) <i class="fas fa-caret-up"></i></td>
										<td class="txt-red">856</td>
										<td class="txt-red">125</td>
										<td class="txt-red">101,200</td>
										<td class="txt-red">1,500,350,000</td>
									</tr>
								<?php endfor; ?>
							</table>
						</div>
					</div>
					<div class="item-info__tab" data-tab="#phan-tich-do-lech">
						<div class="item-info__section">
							<img src="./images/phai-sinh/6.svg">
						</div>
						<div class="item-info__section">
							<img src="./images/phai-sinh/7.svg">
						</div>
					</div>
					<div class="item-info__tab" data-tab="#anh-huong-vn30">
						<div class="item-info__section">
							<img src="./images/phai-sinh/8.svg">
						</div>
					</div>
				</div>
				<div class="item-info__sidebar">
					<div class="item-info__sidebar__nav">
						<a href="#khop-lenh" class="is-active">Khớp lệnh</a>
						<a href="#buoc-gia">Bước giá</a>
					</div>
					<div class="item-info__sidebar__tab is-active" data-tab="#khop-lenh">
						<table>
							<tr>
								<th>Thời gian</th>
								<th>Giá</th>
								<th>KL</th>
								<th>Tổng KL</th>
							</tr>
							<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
								<tr>
									<td class="txt-green txt-right">15:05:20</td>
									<td class="txt-green txt-right">95.00</td>
									<td class="txt-green txt-right">39</td>
									<td class="txt-green txt-right">153</td>
								</tr>
							<?php endfor; ?>
						</table>
					</div>
					<div class="item-info__sidebar__tab" data-tab="#buoc-gia">
						<table>
							<tr>
								<th colspan="2">Dư mua</th>
								<th colspan="2">Dư bán</th>
							</tr>
							<tr>
								<th>Giá</th>
								<th>KL</th>
								<th>Giá</th>
								<th>KL</th>
							</tr>
							<?php for ( $i = 1; $i <= 10; $i++ ) : ?>
								<tr>
									<td class="txt-green txt-right">85.75</td>
									<td class="txt-green txt-right">35</td>
									<td class="txt-green txt-right">87.50</td>
									<td class="txt-green txt-right">36</td>
								</tr>
							<?php endfor; ?>
						</table>
						<div class="item-info__sidebar__stats">
							<div class="bg-green" style="width: 23%">23%</div>
							<div class="bg-red" style="width: 77%">77%</div>
						</div>
					</div>
				</div>
			</div>
		</td>
	</tr>
</table>