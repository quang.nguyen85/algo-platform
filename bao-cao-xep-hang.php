<?php include 'header.php'; ?>
<div class="bc__section">
	<div class="bc__header">
		<div class="bc_row">
			<div class="bc__left">
				<img src="./images/logo-color.png" alt="logo">
				<table>
					<tr>
						<td class="first-col">MWG <span class="txt-right">HOSE</span></td>
						<td class="txt-center">Vốn hóa </br> tỷ</td>
						<td class="txt-center">GTGD </br> tỷ/ngày</td>
						<td class="txt-center">P/E</td>
						<td class="txt-center">P/B</td>
						<td class="txt-center">Cổ tức</td>
						<td class="txt-center">Giá</td>
					</tr>
					<tr>
						<td class="first-col">CTCP đầu tư thế giới di động</td>
						<td class="txt-center">37.090</td>
						<td class="txt-center">58.8</td>
						<td class="txt-center">11.7</td>
						<td class="txt-center">4.3</td>
						<td class="txt-center">1.3 %</td>
						<td class="txt-center">83.7</td>
					</tr>
				</table>
			</div>
			<div class="bc__right">
				<h4>Báo cáo tự động MBS</h4>
				<p>Báo cáo khởi tạo: 14/12/2018 08:40</p>
				<div class="bc_row">
					<div class="bc__left">
						<div class="number">56.7</div>
					</div>
					<div class="bc__right can-nhac-mua">
						<h5>Cân nhắc mua</h5>
						<p>Dữ liệu đến 13/12/2018</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bc__content">
		<div class="bc_row">
			<div class="col-left-30">
				<h4 class="bc-title">Mbs power rank</h4>
				<div class="bc_row inner">
					<div class="col-left-30">
						<div class="number">56.7</div>
					</div>
					<div class="col-right-70 can-nhac-mua">
						<h5>Cân nhắc mua</h5>
						<p>Dữ liệu đến 13/12/2018</p>
					</div>
				</div>
				<div>
					<table>
						<tr>
							<td>Macro occonomic</td>
							<td>7.8</td>
							<td>Induatry</td>
							<td>9.2</td>
							<td>Company</td>
							<td>4.4</td>
						</tr>
					</table>
				</div>
				<div class="col-backghround">
					<p>- Historical Powet Rank (Q1,2,3/2018)</p>
				</div>
				<div class="col-backghround">
					<p>Điểm giá trị A</p>
					<p>Điểm sức khỏe tài chính B</p>
					<p>Điểm thanh khoản b</p>
					<p><i>Ghi chú:</i>Để biết thêm thông tin cụ thể về <b>MBS Power Rank</b> và các mô hình định lượng liên quan vui lòng xem trang 3</p>
				</div>
			</div>
			<div class="col-right-70">
				<h4 class="bc-title">Biến động của cổ phiếu MWG</h4>
				<div class="col-backghround">
					<div class="bc_row">
						<div class="bc-col">
							<img src="./images/chi-tiet-ma/8.svg">
						</div>
						<div class="bc-col">
							<img src="./images/chi-tiet-ma/8.svg">
						</div>
					</div>
					<div class="bc_row">
						<div class="bc-col">
							<img src="./images/chi-tiet-ma/8.svg">
						</div>
						<div class="bc-col">
							<img src="./images/chi-tiet-ma/8.svg">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bc_row">
			<div class="col-left-70">
				<h4 class="bc-title">Cơ sở đánh giá công ty của MSB POWER RANK</h4>
				<div class="bc_row">
					<div class="col-left-70 col-backghround">
						<p>Đánh giá công ty của MBS Power Rank được cấu thành từ 5 khía cạnh chính:</p>
						<img src="./images/chi-tiet-ma/8.svg">
					</div>
					<div class="col-right-30 col-backghround">
						<p><i>Ghi chú:</i> Đánh giá công ty của <b>MBS Power Rank</b> theo 5 khía cạnh chính trong mối quan hệ với các công ty cùng ngành theo thang điểm từ 0-10.</p>
						<p>Để biết thêm thông tin cụ thể vui lòng xem thêm trang 3.</p> 
					</div>
				</div>
				<div>
					<h4 class="bc-title">Đánh giá rủi ro</h4>
					<div class="col-backghround">
						<p><b>Chất lượng BCTC:</b> M-score = -0.67</p>
						<p><b>Beta:</b>  1.27</p>
						<p><b>Rủi ro căng thẳng tài chính:</b> Z-score = 2.8</p>
						<p><b>5% VaR (1 ngày giao dịch):</b> <span class="txt-red">-4.2%</span></p>
						<p><b>Tương quan VN-Index cùng giai đoạn:</b></p>
						<p><b>5% VaR (1 ngày giao dịch):</b> <span class="txt-red">-2.7%</span></p>
					</div>
				</div>
			</div>
			<div class="col-right-30">
				<h4 class="bc-title">Tin tức doanh nghiệp</h4>
				<div class="col-backghround">
					<p>MWG: DC Developing Markets Strategies PLC đã bán 266.666 cp (10/12/2018 10:49)</p>
					<p>MWG: 14.12.2018, giao dịch 107.597.146 cp niêm yết bổ sung (07/12/2018 17:28)</p>
					<p>MWG: Thay đổi giấy chứng nhận đăng ký doanh nghiệp lần 19 (06/12/2018 16:52)</p>
					<p>MWG: 7.12.2018, niêm yết bổ sung 107.597.146 cp (06/12/2018 09:08)</p>
					<p>Thế giới Di động (MWG) sắp phát hành 13 triệu cổ phiếu ESOP (02/12/2018 14:20)</p>
					<p>MWG: Nghị quyết HĐQT v/v phát hành cổ phiếu ESOP (29/11/2018 17:36)</p>
				</div>
			</div>
		</div>
	</div>
</div>
<p style="page-break-after:always;"></p>
<div class="bc__section">
	<div class="bc__header">
		<div class="bc_row">
			<div class="bc__left">
				<img src="./images/logo-color.png" alt="logo">
				<table>
					<tr>
						<td class="first-col">MWG <span class="txt-right">HOSE</span></td>
						<td class="txt-center">Vốn hóa </br> tỷ</td>
						<td class="txt-center">GTGD </br> tỷ/ngày</td>
						<td class="txt-center">P/E</td>
						<td class="txt-center">P/B</td>
						<td class="txt-center">Cổ tức</td>
						<td class="txt-center">Giá</td>
					</tr>
					<tr>
						<td class="first-col">CTCP đầu tư thế giới di động</td>
						<td class="txt-center">37.090</td>
						<td class="txt-center">58.8</td>
						<td class="txt-center">11.7</td>
						<td class="txt-center">4.3</td>
						<td class="txt-center">1.3 %</td>
						<td class="txt-center">83.7</td>
					</tr>
				</table>
			</div>
			<div class="bc__right">
				<h4>Báo cáo tự động MBS</h4>
				<p>Báo cáo khởi tạo: 14/12/2018 08:40</p>
				<div class="bc_row">
					<div class="bc__left">
						<div class="number">56.7</div>
					</div>
					<div class="bc__right can-nhac-mua">
						<h5>Cân nhắc mua</h5>
						<p>Dữ liệu đến 13/12/2018</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bc__content">
		<div class="bc_row">
			<div class="col-50">
				<h4 class="bc-title">GÓC NHÌN PHÂN TÍCH MBS </h4>
				<div class="col-backghround txt-red">
					<p class="txt-center">Analyst: Mr. ABC</p>
					<p class="txt-red">- Phân tích cơ bản gần nhất</p>
					<p class="txt-red">- Phân tích kỹ thuật</p>
					<p class="txt-red">- Khuyến nghị kỹ thuật hiện tại</p>
				</div>
			</div>
			<div class="col-50">
				<h4 class="bc-title">Hồ sơ doanh nghiệp</h4>
				<div class="col-backghround">
					<p>Công ty Cổ phần Đầu tư Thế Giới Di Động (MWG) niêm yết và giao dịch trên Sở Giao dịch Chứng khoán Tp.HCM (HOSE) từ tháng 07/2014. Công ty quản lý vận hành các chuỗi bán lẻ thegioididong.com, Điện Máy Xanh (bao gồm chuỗi Trần Anh), Bách Hoá Xanh; cùng trang thương mại điện tử Vuivui.com với mạng lưới hơn 2.000 cửa hàng trên toàn quốc. Thegioididong.com được thành lập từ 2004 là chuỗi bán lẻ thiết bị di động có thị phần số 1 Việt Nam với hơn 1.000 siêu thị. Điện Máy Xanh ra đời cuối 2010, là chuỗi bán lẻ các sản phẩm điện tử tiêu dùng (điện tử, điện lạnh và gia dụng) có thị phần số 1 Việt Nam với gần 700 cửa hàng hiện diện tại 63 tỉnh thành trên khắp Việt Nam. Bách Hóa Xanh được đưa vào thử nghiệm từ cuối năm 2015, là chuỗi cửa hàng chuyên bán lẻ thực phẩm tươi sống (thịt cá, rau củ, trái cây…) và nhu yếu phẩm với hơn 350 siêu thị tại Tp. Hồ Chí Minh.</p>
				</div>
			</div>
		</div>
		<div class="bc_row">
			<div class="col-left-70">
				<h4 class="bc-title">Kết quả kinh doanh, dòng tiền và EPS/ cổ tức</h4>
				<div class="bc_row">
					<div class="bc-col col-backghround col-inner">
						<h5 class="txt-center">Kết quả kinh doanh</h5>
						<img src="./images/chi-tiet-ma/8.svg">
					</div>
					<div class="bc-col col-backghround col-inner">
						<h5 class="txt-center">Dòng tiền</h5>
						<img src="./images/chi-tiet-ma/8.svg">
					</div>
					<div class="bc-col col-backghround  col-inner">
						<h5 class="txt-center">EPS và cổ tức</h5>
						<img src="./images/chi-tiet-ma/8.svg">
					</div>
				</div>
				<div class="bc_row">
					<div class="col-left-70">
						<h4 class="bc-title">So sánh các công ty cùng ngành</h4>
						<table class="inner">
							<tr>
								<td class="txt-red">Peers</td>
								<td>Vốn hóa</td>
								<td>PE</td>
								<td>PB</td>
								<td>ROE</td>
								<td>BiênLN</td>
								<td>Vay/VSh</td>
								<td>Div.YId%</td>
							</tr>
							<tr>
								<td>Top100</td>
								<td>37,090</td>
								<td>11.7</td>
								<td>4.3</td>
								<td>44%</td>
								<td>35%</td>
								<td>1.5</td>
								<td>1%</td>
							</tr>
							<tr>
								<td>Ngành</td>
								<td>37,090</td>
								<td>11.7</td>
								<td>4.3</td>
								<td>44%</td>
								<td>35%</td>
								<td>1.5</td>
								<td>1%</td>
							</tr>
							<tr>
								<td>PET</td>
								<td>37,090</td>
								<td>11.7</td>
								<td>4.3</td>
								<td>44%</td>
								<td>35%</td>
								<td>1.5</td>
								<td>1%</td>
							</tr>
							<tr>
								<td>DGW</td>
								<td>37,090</td>
								<td>11.7</td>
								<td>4.3</td>
								<td>44%</td>
								<td>35%</td>
								<td>1.5</td>
								<td>1%</td>
							</tr>
							<tr>
								<td>PSD</td>
								<td>37,090</td>
								<td>11.7</td>
								<td>4.3</td>
								<td>44%</td>
								<td>35%</td>
								<td>1.5</td>
								<td>1%</td>
							</tr>
							<tr>
								<td>TMB</td>
								<td>37,090</td>
								<td>11.7</td>
								<td>4.3</td>
								<td>44%</td>
								<td>35%</td>
								<td>1.5</td>
								<td>1%</td>
							</tr>
						</table>
					</div>
					<div class="col-right-30">
						<h4 class="bc-title">Top cổ đông lớn</h4>
						<table class="inner">
							<tr>
								<td class="txt-red txt-right"><b>Cổ đông lớn</b></td>
							</tr>
							<tr class="txt-right">
								<td>Đầu tư thế giới bán lẻ (4.36%)</td>
							</tr>
							<tr class="txt-right">
								<td>Đầu tư thế giới bán lẻ (4.36%)</td>
							</tr>
							<tr class="txt-right">
								<td>Đầu tư thế giới bán lẻ (4.36%)</td>
							</tr>
							<tr class="txt-right">
								<td>Đầu tư thế giới bán lẻ (4.36%)</td>
							</tr>
							<tr class="txt-right">
								<td>Đầu tư thế giới bán lẻ (4.36%)</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="col-right-30">
				<h4 class="bc-title">Các chỉ tiêu quan trọng</h4>
				<div class="col-backghround">
					<p>Tăng trưởng giá trị chất lượng</p>
					<table class="tb-chi-tieu">
						<tr class="tr-heading">
							<td>Tăng trưởng (trailing 12M)</td>
							<td></td>
							<td>Ngành</td>
							<td>Thị trường</td>
						</tr>
						<tr>
							<td>PE</td>
							<td>13.4</td>
							<td>13.30</td>
							<td>15.7</td>
						</tr>
						<tr>
							<td>Tăng trưởng EPS</td>
							<td class="txt-red"> -28.76%</td>
							<td class="txt-red"> -14.65%</td>
							<td>2.18%</td>
						</tr>
						<tr>
							<td>Lợi tức cổ tức</td>
							<td>1.73%</td>
							<td>1.78%</td>
							<td>2.66%</td>
						</tr>
						<tr class="tr-heading">
							<td>Giá trị (trailing 12M)</td>
							<td></td>
							<td>Ngành</td>
							<td>Thị trường</td>
						</tr>
						<tr>
							<td>PB</td>
							<td>5.18</td>
							<td>4.33</td>
							<td>2.33</td>
						</tr>
						<tr>
							<td>Giá/giá trị số sách hữu hình</td>
							<td>3.83</td>
							<td>3.52</td>
							<td>2.23</td>
						</tr>
						<tr>
							<td>P/FCF</td>
							<td class="txt-red">-23.76</td>
							<td class="txt-red">-24.8</td>
							<td>4,825.63</td>
						</tr>
						<tr>
							<td>PS</td>
							<td>0.44</td>
							<td>0.40</td>
							<td>1.39</td>
						</tr>
						<tr class="tr-heading">
							<td>Niên độ gần nhất với năm trước</td>
							<td></td>
							<td>Ngành</td>
							<td>Thị trường</td>
						</tr>
						<tr>
							<td>Tẳng trưởng doanh thu</td>
							<td>37.00%</td>
							<td>32.99%</td>
							<td>17.58%</td>
						</tr>
						<tr>
							<td>Tăng trưởng EPS</td>
							<td class="txt-red">-28.76%</td>
							<td class="txt-red">-14.65%</td>
							<td>2.18%</td>
						</tr>
						<tr class="tr-heading">
							<td>Tăng trưởng kép hàng năm (3 năm)</td>
							<td></td>
							<td>Ngành</td>
							<td>Thị trường</td>
						</tr>
						<tr>
							<td>Tăng trưởng doanh thu</td>
							<td>46.52%</td>
							<td>41.97%</td>
							<td>15.98%</td>
						</tr>
						<tr>
							<td>Tăng trưởng EPS</td>
							<td>3.23%</td>
							<td>6.08%</td>
							<td class="txt-red">-0.90%</td>
						</tr>
						<tr>
							<td>Tăng trưởng DPS</td>
							<td></td>
							<td>35.18%</td>
							<td>0.87%</td>
						</tr>
						<tr class="tr-heading">
							<td>Chất lượng</td>
							<td></td>
							<td>Ngành</td>
							<td>Thị trường</td>
						</tr>
						<tr>
							<td>ROCE</td>
							<td>38.56%</td>
							<td>32.21%</td>
							<td>36.60%</td>
						</tr>
						<tr>
							<td>ROE</td>
							<td>38.25%</td>
							<td>33.33%</td>
							<td>15.45%</td>
						</tr>
					</table>
					<p>Quán tính bổ sung</p>
					<table class="tb-quan-tinh">
						<tr class="tr-heading">
							<td>Sức mạnh tương đương</td>
							<td></td>
						</tr>
						<tr>
							<td>1 Tháng</td>
							<td class="txt-red">-3.06%</td>
						</tr>
						<tr>
							<td>3 Tháng</td>
							<td>0.31%</td>
						</tr>
						<tr>
							<td>1 năm</td>
							<td>12.00%</td>
						</tr>
						<tr class="tr-heading">
							<td>Thay đổi khối lượng</td>
							<td></td>
						</tr>
						<tr>
							<td>10 ngày vs 3 tháng</td>
							<td class="txt-red">-3.06%</td>
						</tr>
						<tr>
							<td>10 ngày với 6 tháng</td>
							<td>0.31%</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<p style="page-break-after:always;"></p>
<div class="bc__section">
	<div class="bc__header">
		<div class="bc_row">
			<div class="bc__left">
				<img src="./images/logo-color.png" alt="logo">
				<table>
					<tr>
						<td class="first-col">MWG <span class="txt-right">HOSE</span></td>
						<td class="txt-center">Vốn hóa </br> tỷ</td>
						<td class="txt-center">GTGD </br> tỷ/ngày</td>
						<td class="txt-center">P/E</td>
						<td class="txt-center">P/B</td>
						<td class="txt-center">Cổ tức</td>
						<td class="txt-center">Giá</td>
					</tr>
					<tr>
						<td class="first-col">CTCP đầu tư thế giới di động</td>
						<td class="txt-center">37.090</td>
						<td class="txt-center">58.8</td>
						<td class="txt-center">11.7</td>
						<td class="txt-center">4.3</td>
						<td class="txt-center">1.3 %</td>
						<td class="txt-center">83.7</td>
					</tr>
				</table>
			</div>
			<div class="bc__right">
				<h4>Báo cáo tự động MBS</h4>
				<p>Báo cáo khởi tạo: 14/12/2018 08:40</p>
				<div class="bc_row">
					<div class="bc__left">
						<div class="number">56.7</div>
					</div>
					<div class="bc__right can-nhac-mua">
						<h5>Cân nhắc mua</h5>
						<p>Dữ liệu đến 13/12/2018</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bc__content">
		<h4 class="bc-title">Thuyết minh</h4>
		<div class="thuyet-minh">
			<ul>
				<li>MBS Power Rank là phương pháp định giá được phát triển bởi MBS với ý tưởng đầu tư là tìm những công ty vượt trội trong ngành theo cả 5 khía cạnh chính
					<ol>
						<li>Quy mô:  Chỉ tiêu này bao gồm các chỉ tiêu chính nhằm xác định quy mô một công ty tương đối với ngành như quy mô vốn, doanh thu, tổng tài sản… </li>
						<li>Thanh khoản:  Chỉ tiêu này bao gồm các chỉ tiêu chính nhằm đánh giá khả năng thanh khoản dùng các chỉ tiêu như chỉ số thanh toán nhanh, chỉ số thanh toán hiện thời…</li>
						<li>Hiệu quả hoạt động: Chỉ tiêu này gồm các chỉ tiêu nhằm đánh giá hiệu quả hoạt động của công ty tương đối so với các công ty trong ngành như ROE, ROA, vòng quay tổng tài sản… </li>
						<li>Cơ cấu vốn: Chỉ tiêu này xem xét sự lành mạnh trong việc sử dụng vốn chủ sở hữu cũng như đòn bẩy dùng các chỉ tiêu như tổng nợ / tổng vốn chủ sở hữu…</li>
						<li>Định giá: Chỉ tiêu này đánh giá mức độ “rẻ” tương đối của giá cổ phiếu theo các chỉ tiêu nổi trội như P/E, P/B và phương pháp định giá chuyên gia của MBS. </li>
					</ol>
					Các chỉ tiêu trên được đánh giá cho tất cả công ty trong ngành theo thang điểm từ 1-10 Bên cạnh đánh giá công ty, MBS Power Rank còn đánh giá và chấm điểm tình trạng kinh tế vĩ mô hiện tại cũng như ngành hoạt động của công ty để đưa ra điểm số và khuyến nghị tương ứng. Cụ thể như sau:
				</li>
				<li>Điểm giá trị đánh giá các cổ phiếu dùng 2 chỉ tiêu chính là Earnings Yield (Suất sinh lời) và Returns on Capital (Lợi tức trên vốn đầu tư) trên thang điểm A+, A, A-, B+, B, B-, C+, C, C- và D. Nguyên tắc đầu tư nền tảng của điểm giá trị là đầu tư giá trị dành cho các nhà đầu tư theo đuổi trường phái này.
					<ul>
						<li><40: Khuyến nghị bán lập tức (Strong sell).</li>
						<li>[40 ; 50]: Khuyến nghị cân nhắc bán.</li>
						<li>(50 ; 60]: Khuyến nghị  cân nhắc mua.</li>
						<li>>60: Khuyến nghị mua ngay (Strong buy).</li>
					</ul>
				</li>
				<li>Điểm sức khỏe tài chính đánh giá các cổ phiếu dùng 9 tiêu chí chia thành 3 nhóm Profitability (Sinh lời), Leverage, Liquidity and Source of Funds (Đòn bẩy, Thanh khoản và Nguồn vốn), Operating Efficiency (Hiệu quả hoạt động) trên thang điểm A+, A,  A-, B+, B, B-, C+, C, C- và D.</li>
				<li>Điểm thanh khoản đánh giá thanh khoản một mã cổ phiếu theo 4 mức (A,B,C,D) như sau:
					<ul>
						<li>A (Rất cao)     : Trên 1 triệu cổ phiếu/ phiên</li>
						<li>B (Khá cao)     : Từ 100.000 đến 1 triệu cổ phiếu/ phiên</li>
						<li>C(Trung bình)   : Từ 10.000 đến 100.000 cổ phiếu/ </li>
						<li>D (Thấp)        : Dưới 10.000 cổ phiếu/ phiên</li>
					</ul>
					<i>Thanh khoản tính trung bình 1 tháng</i>
				</li>
				<li>Chất lượng báo cáo tài chính được đánh giá dùng chỉ tiêu M-score. Khi M > -2.22:  khả năng BCTC đang bị thao túng.</li>
				<li>Rủi ro căng thẳng tài chính (Financial Distress): đánh giá rủi ro nợ của công ty và dự đoán được nguy cơ phá sản trong tương lai gần dùng chỉ tiêu Z-score. 
				Khi Z > 2.99: Công ty có tài chính lành mạnh
				Khi 1.81< Z <2.99: Công ty không có vấn đề trong ngắn hạn, tuy nhiên cần phải xem xét điều kiện tài chính một cách thận trọng
				Khi Z <=1.81: Công ty có vấn đề nghiêm trọng về tài chính.
				</li>
				<li>Rủi ro của cổ phiếu được đánh giá theo 2 khía cạnh chính:
					<ol>
						<li>Beta:  hệ số đo lường mức độ rủi ro hệ thống của cổ phiếu thể hiện mức độ tương quan của biến động giá cổ phiếu so với sự biến động chung của thị trường. Hệ số beta của thị trường mặc định luôn bằng 1.</li>
						<li>5% VaR 1 ngày: chỉ có 5% khả năng thua lỗ trong ngày giao dịch vượt qua mức VaR được ghi nhận trong báo cáo này. </li>
					</ol>
				</li>
			</ul>
		</div>
	</div>
</div>
<p style="page-break-after:always;"></p>
<div class="bc__section">
	<div class="bc__header">
		<div class="bc_row">
			<div class="bc__left">
				<img src="./images/logo-color.png" alt="logo">
				<table>
					<tr>
						<td class="first-col">MWG <span class="txt-right">HOSE</span></td>
						<td class="txt-center">Vốn hóa </br> tỷ</td>
						<td class="txt-center">GTGD </br> tỷ/ngày</td>
						<td class="txt-center">P/E</td>
						<td class="txt-center">P/B</td>
						<td class="txt-center">Cổ tức</td>
						<td class="txt-center">Giá</td>
					</tr>
					<tr>
						<td class="first-col">CTCP đầu tư thế giới di động</td>
						<td class="txt-center">37.090</td>
						<td class="txt-center">58.8</td>
						<td class="txt-center">11.7</td>
						<td class="txt-center">4.3</td>
						<td class="txt-center">1.3 %</td>
						<td class="txt-center">83.7</td>
					</tr>
				</table>
			</div>
			<div class="bc__right">
				<h4>Báo cáo tự động MBS</h4>
				<p>Báo cáo khởi tạo: 14/12/2018 08:40</p>
				<div class="bc_row">
					<div class="bc__left">
						<div class="number">56.7</div>
					</div>
					<div class="bc__right can-nhac-mua">
						<h5>Cân nhắc mua</h5>
						<p>Dữ liệu đến 13/12/2018</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bc__content">
		<h4 class="bc-title">Miến trách</h4>
		<div class="mien-trach">
			<h4>CÔNG TY CỔ PHẦN CHỨNG KHOÁN MB (MBS)</h4> 
			<p>Được thành lập từ tháng 5 năm 2000 bởi Ngân hàng TMCP Quân đội (MB), Công ty CP Chứng khoán MB (MBS) là một trong 5 công ty chứng khoán đầu tiên tại Việt Nam. Sau nhiều năm không ngừng phát triển, MBS đã trở thành một trong những công ty chứng khoán hàng đầu Việt Nam cung cấp các dịch vụ bao gồm: môi giới, nghiên cứu và tư vấn đầu tư, nghiệp vụ ngân hàng đầu tư và các nghiệp vụ thị trường vốn.</p>
			<p>Mạng lưới chi nhánh và các phòng giao dịch của MBS đã được mở rộng và hoạt động có hiệu quả tại nhiều thành phố trọng điểm như Hà Nội, TP, HCM, Hải Phòng và các vùng chiến lược khác, Khách hàng của MBS bao gồm các nhà đầu tư cá nhân và tổ chức, các tổ chức tài chính và doanh nghiệp. Là thành viên Tập đoàn MB bao gồm các công ty thành viên như: Công ty CP Quản lý Quỹ đầu tư MB (MB Capital), Công ty CP Địa ốc MB (MB Land), Công ty Quản lý nợ và Khai thác tài sản MB (AMC), Công ty CP Việt R.E.M.A.X (VIET R.E.M), Công ty Tài chính TNHH MB Shinsei (MS Finance). MBS có nguồn lực lớn về con người, tài chính và công nghệ để có thể cung cấp cho Khách hàng các sản phẩm và dịch vụ phù hợp mà rất ít các công ty chứng khoán khác có thể cung cấp.
			MBS tự hào được nhìn nhận là:
			Công ty môi giới hàng đầu, đứng đầu thị phần môi giới từ năm 2009.
			Công ty nghiên cứu có tiếng nói trên thị trường với đội ngũ chuyên gia phân tích có kinh nghiệm, cung cấp các sản phẩm nghiên cứu về kinh tế và thị trường chứng khoán; và
			Nhà cung cấp đáng tin cậy các dịch vụ về nghiệp vụ ngân hàng đầu tư cho các công ty quy mô vừa.</p>
			<h4>MBS HỘI SỞ</h4>
			<ul>
				<li>Tòa nhà MB, số 3 Liễu Giai, Ba Đình, Hà Nội</li>
				<li>ĐT: + 84 4 3726 2600 - Fax: +84 3726 2601</li>
				<li>Website: www.mbs.com.vn</li>
			</ul>
			<p><b>Tuyên bố miễn trách nhiệm:</b> Bản quyền năm 2018 thuộc về Công ty CP Chứng khoán MB (MBS), Những thông tin sử dụng trong báo cáo được thu thập từ những nguồn đáng tin cậy và MBS không chịu trách nhiệm về tính chính xác của chúng, Quan điểm thể hiện trong báo cáo này là của (các) tác giả và không nhất thiết liên hệ với quan điểm chính thức của MBS, Không một thông tin cũng như ý kiến nào được viết ra nhằm mục đích quảng cáo hay khuyến nghị mua/bán bất kỳ chứng khoán nào, Báo cáo này không được phép sao chép, tái bản bởi bất kỳ cá nhân hoặc tổ chức nào khi chưa được phép của MBS.</p>
		</div>
	</div>
</div>
<p style="page-break-after:always;"></p>