<?php include 'header.php'; ?>

<div class="fixed-components">
	<?php $menu_title = 'Chứng quyền'; ?>
	<?php include 'site-header.php'; ?>
	<?php include 'charts.php'; ?>
	<?php include 'stock-tables/header-no-tabs.php'; ?>
</div>

<div class="stock-tables__table">
	<div class="stock-table table--active chung-quyen">
		<?php
		include( 'stock-tables/chung-quyen/header.php' );
		include( 'stock-tables/chung-quyen/body.php' );
		?>
	</div>
</div>

<?php include 'footer.php'; ?>