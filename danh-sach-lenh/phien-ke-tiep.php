<div class="dsl__dropdown">
	<a href="#" class="dsl__dropdown__toggle"><i class="fas fa-plus"></i>Danh sách lệnh cho phiên kế tiếp</a>
	<div class="dsl__dropdown__body">
		<table class="dsl__dropdown__table">
			<colgroup>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col class="col-kenh">
				<col class="col-huy">
			</colgroup>
			<thead>
				<tr>
					<th>Giờ nhập</th>
					<th>Tài khoản</th>
					<th>Loại GD</th>
					<th>Mã CK</th>
					<th>KL</th>
					<th>Giá</th>
					<th>Kênh</th>
					<th>Hủy</th>
				</tr>
			</thead>
			<tbody>
				<?php for( $i = 1; $i <= 4; $i++ ) : ?>
					<tr>
						<td class="txt-right">12:05:50</td>
						<td class="txt-right">12191314</td>
						<td>Bán</td>
						<td class="txt-yellow">NVH</td>
						<td class="txt-right">912</td>
						<td class="txt-pink txt-right">512</td>
						<td>Kênh</td>
						<td>Hủy</td>
					</tr>
				<?php endfor; ?>
			</tbody>
		</table>
	</div>
</div>