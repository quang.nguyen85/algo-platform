<div class="dsl__dropdown">
	<a href="#" class="dsl__dropdown__toggle"><i class="fas fa-plus"></i>Giỏ lệnh</a>
	<div class="dsl__dropdown__body">
		<form class="dsl__dropdown__filter d-flex flex-wrap">
			<div class="table__filter__item">
				<label>Loại GD</label>
				<select>
					<option value="">Mua</option>
					<option value="">Bán</option>
				</select>
			</div>
			<div class="table__filter__item">
				<label>Tài khoản</label>
				<input type="text" name="" value="" placeholder="">
			</div>
			<div class="table__filter__item">
				<label>Mã CK</label>
				<input type="text" name="" value="" placeholder="">
			</div>
			<button type="submit" class="btn btn--primary">Tìm kiếm</button>
		</form>
		<table class="dsl__dropdown__table dsl__dropdown__table--gio-lenh">
			<colgroup>
				<col class="col-stt">
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col>
				<col class="col-trong-ngoai">
				<col>
				<col>
				<col>
				<col class="col-stt">
			</colgroup>
			<thead>
				<tr>
					<th class="txt-center">STT</th>
					<th>Giờ nhập</th>
					<th>Tài khoản</th>
					<th>Loại GD</th>
					<th>Mã CK</th>
					<th>KL</th>
					<th>Giá</th>
					<th>Trạng thái</th>
					<th>Giá trị lệnh</th>
					<th>Ký quỹ</th>
					<th>Kênh</th>
					<th>Kích hoạt</th>
					<th><input class="dsl__dropdown__checkall" type="checkbox" name="" value=""></th>
				</tr>
			</thead>
			<tbody>
				<?php for( $i = 1; $i <= 20; $i++ ) : ?>
					<tr>
						<td class="txt-center"><?= $i ?></td>
						<td class="txt-right">12:05:50</td>
						<td class="txt-right">12191314</td>
						<td>Bán</td>
						<td>NVH</td>
						<td class="txt-right">912</td>
						<td class="txt-pink txt-right">512</td>
						<td>Đang chờ</td>
						<td class="txt-right">1256</td>
						<td class="txt-right">23%</td>
						<td>Kênh</td>
						<td>Kích hoạt</td>
						<td><input class="dsl__dropdown__checkbox" type="checkbox" name="" value=""></td>
					</tr>
				<?php endfor; ?>
			</tbody>
		</table>
		<div class="dsl__dropdown__footer d-flex flex-wrap">
			<a href="#" class="dsl__dropdown__reset-checkbox  btn btn--red">Hủy lệnh</a>
			<a href="#" class="btn btn--primary">Kích hoạt</a>
			<div class="dsl__dropdown__pagination">
				<span class="first"><i class="fas fa-step-backward"></i></span>
				<span class="prev"><i class="fas fa-chevron-left"></i></span>
				<input class="dropdown-pagination__page-number" type="number" min="1" value="1">
				<span class="next"><i class="fas fa-chevron-right"></i></span>
				<span class="last"><i class="fas fa-step-forward"></i></span>
				<input class="dropdown-pagination__row-number" type="number" min="1" value="5">
			</div>
		</div>
	</div>
</div>