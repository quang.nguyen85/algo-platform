<header class="site-header">
	<div class="header__left">
		<div class="site-icon">
			<img src="./images/logo-en.png" alt="logo">
		</div>
		<div class="header__search">
			<form role="search" method="get" class="search-form" action="">
				<label>
					<input class="search-field input-txt--dark" id="view-code" placeholder="Xem chi tiết mã..." name="s" type="search">
				</label>
				<button type="submit" class="search-submit" id="view-code-submit"><i class="fas fa-search"></i></button>
			</form>
		</div>
		<div class="header__time">
			<span class="hour">10:05:00</span>
		</div>
	</div>
	<div class="header__center">
		<div class="news">
			<a href="#">Government plans to slash stake in Vietnam Airlines to 51 percent</a>
			<a href="#">"A joint venture with Colgate was my biggest mistake"</a>
		</div>
	</div>
	<div class="header__right">
		<ul class="header__navigation">
			<li class="dropdown">
				<a class="dropdown-toggle is-active" href="#">Underlyings <i class="fas fa-angle-down"></i></a>
				<ul class="dropdown-menu">
					<li><a href="./">Underlyings</a></li>
					<li><a href="./phai-sinh.php">Derivatives</a></li>
					<li><a href="#">Covered Warrants</a></li>
					<li><a href="#">Bonds</a></li>
					<li><a href="#" target="_blank">Old Version</a></li>
				</ul>
			</li>
			<li><a href="./chuyen-dong-thi-truong.php">Market Watch</a></li>
			<li><a href="#">MBS Platform</a></li>
		</ul>
		<div class="header__language dropdown">
			<a class="dropdown-toggle" href="#"><img src="./images/gb.svg" alt="gb"></a>
			<ul class="dropdown-menu">
				<li><a href="./"><img src="./images/vn.svg" alt="vn"> Tiếng Việt</a></li>
				<li><a href="./en.php"><img src="./images/gb.svg" alt="en"> English</a></li>
				<li><a href="./en.php"><img src="./images/cn.svg" alt="cn"> 中文</a></li>
				<li><a href="./en.php"><img src="./images/kr.svg" alt="cn"> 한국어</a></li>
				<li><a href="./en.php"><img src="./images/jp.svg" alt="jp"> 日本語</a></li>
			</ul>
		</div>
		<div class="header__actions">
			<a href="#" class="btn btn--primary init-dat-lenh-popup">Place Order</a>
			<div class="account dropdown">
				<div class="account__menu dropdown-toggle">
					<div class="account__image"><i class="fas fa-user"></i></div>
				</div>
				<ul class="dropdown-menu">
					<li><a href="#"><i class="fas fa-user"></i> Nguyen Van Tuan</a></li>
					<li><a href="#"><i class="far fa-file-alt"></i> 64215384</a></li>
					<li><a href="#"><i class="fas fa-key"></i> Change Password</a></li>
					<li><a href="#"><i class="fas fa-sign-out-alt"></i> Logout</a></li>
				</ul>
			</div>
		</div>
	</div>
</header>
